.. _sect_introduction_technologies:

============
Technologies
============

This section introduces the technologies that are used to get CTGames web apps running.
As a CTGames games developer you'll interact directly with the Python programming language, the CTGames framework, the SVG image format, and the GSAP library.
However, there are several important technologies used under the hood of these technologies that are not exposed to the game developer.

Brython is a transpiler from Python to JavaScript, that allows Python programs to be run in the browser, and allows Python programs to manipulate the `DOM <https://en.wikipedia.org/wiki/Document_Object_Model>`_.
Furthermore, Brython is also written in JavaScript so the transpilation operation can be done live in the browser.
The GSAP animation library is also implemented using JavaScript, and this might be evident to some by the style of the GSAP API.

The teacher portal part of the CTGames framework is implemented using a Flask server, an Nginx web server, and a PostgreSQL database, each in its own Docker container.
More details on the teacher portal can be found in the chapter ":ref:`Teacher portal development <chap_portal_development>`".


------
Python
------

`Python`_ is a high-level programming language. There are many articles, tutorials, and textbooks about Python. You might be interested in this `Wikipedia article on Python`_. There are many good textbooks available for free online. One old, but still good one is "Think Python - How to Think Like a Computer Scientist," 2nd Edition, Green Tea Press, by Allen B. Downey. It has `HTML and PDF versions <https://greenteapress.com/wp/think-python-2e/>`_ available and an `interactive online version <https://runestone.academy/runestone/books/published/thinkcspy/index.html>`_ that includes videos and allows you to execute examples of code from the book directly in the web page.

Please note, learning Python from a book takes a long time. If you are a reasonably competent programmer in some other imperative programming language (such as Java or C) then you should just jump in and start programming a CTGames game using the supplied template and code from other games as a guide. Online articles and tutorials, and websites such as `Stack Overflow <https://stackoverflow.com/questions/tagged/python?tab=Votes>`_, will be valuable aids to help you understand a topic more deeply (such as the :py:class:`range` class in Python) at the point in time that you specifically need it.

Python has several implementations, of which the most common is cpython (an implementation using the C programming language). The Python we use with the CTGames framework is called Brython (see section below).


Lists of free online Python books
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The links in the list of lists below work as of December 2020. They are lists I've come across, or Google has ranked highly, rather than the best lists I could find with an in-depth search. By their nature, some links may have broken by the time you read this, or some books may not be free any more.

.. admonition:: Free online Python books

    - https://javinpaul.medium.com/best-python-books-a93d1a0d842d (October 2020)
    - https://hackr.io/blog/best-python-books-for-beginners-and-advanced-programmers (September 2020)
    - https://medium.com/javarevisited/my-favorite-books-to-learn-python-in-depth-77465633b46e (April 2020)
    - https://www.guru99.com/best-python-books.html (January 2020)
    - https://www.pythonkitchen.com/legally-free-python-books-list/ (January 2020)
    - https://realpython.com/best-python-books/ (July 2019)
    - https://www.techrepublic.com/article/learning-python-best-free-books-tutorials-and-videos/ (July 2019)
    - https://stackabuse.com/the-best-python-books-for-all-skill-levels/ (February 2018)
    - https://www.java67.com/2017/05/top-7-free-python-programming-books-pdf-online-download.html (July 2017)
    - https://codeburst.io/15-free-ebooks-to-learn-python-c299943f9f2c (March 2017)


-----------------
CTGames framework
-----------------

Following these topics in these docs in the order they are presented is the best way to learn how to use the CTGames framework to develop computational thinking games.
However, if you want to skip ahead to better understand how the framework works in advance of learning how to use it, see Chapter ":ref:`CTGames framework <chap_ctgames_framework>`".


-------
Brython
-------

`Brython`_'s goal is to facilitate Python as an alternative to JavaScript as the scripting language for web browsers.

Brython is a transpiler from Python to JavaScript, that allows Python programs to be run in the browser, and allows Python programs to manipulate the `DOM <https://en.wikipedia.org/wiki/Document_Object_Model>`_.

The command-line versions of CTGames games are written in Python so it is natural to want to use the same programming language for the web app versions.
There are other approaches to running Python in the browser.
Brython was chosen at the time (2016) as its aim was to have as close a match as possible to the official Python language standard implementation cpython.
As of 2022, although there are promising developments that use Web Assembly for example, there there is no compelling reason to switch from Brython to an alternative technology.

Useful resources to learn about Brython
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The CTGames framework provides a reasonably clean abstraction layer between the game code and the underlying web app technologies, so understanding `Brython`_ is not necessary to use the CTGames framework to develop web app games.
However, if you want to learn about Brython, the following list of resources is current as of 2022.
As always, :abbr:`GIYF (Google is your friend)` for the most up-to-date links to resources.

    - TODO
    - TODO
    - TODO


---
SVG
---

TODO: :abbr:`SVG (scalable vector graphics)` is...


----
GSAP
----

TODO: GSAP is...

.. _Python: https://www.python.org/
.. _Brython: https://www.brython.info/
.. _Wikipedia article on Python: https://en.wikipedia.org/wiki/Python_(programming_language)
