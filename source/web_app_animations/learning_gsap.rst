.. _sect_learning_gsap:

==========================================
External resources for learning about GSAP
==========================================

We use the `GreenSock GSAP JavaScript animation library <https://greensock.com/gsap/>`_ to animate SVG images in the CTGames framework.

Many web-based resources are available to learn about the capabilities of GSAP, and how to use it. The following are some of the top search results as of early 2022. Using GSAP through Brython mirrors exactly how to use it through JavaScript, so all tutorials/blogs/books about GSAP are potentially useful.

.. tip:: Make sure to consult this section in the context of the examples in section ":ref:`sect_animations_basic`". It may be more efficient to first try to modify one of the examples in that section to get what you need. If you don't find an example that matches your use case, or the example you modify has different behaviour compared to what you want, or you get a syntax error when using the GSAP library, then come back to this section.

.. tip:: GSAP received a major update with `GSAP version 3 in November 2019 <https://greensock.com/3/>`_. Any tutorials/blogs/books written before that date may use outdated syntax or more verbose ways to achieving the same result. For example, any code that uses objects :class:`TweenLite` or :class:`TimelineMax` would pre-GSAP version 3 code.

.. seealso:: For an analysis of the different web app animation technologies considered for CTGames before GSAP was chosen, see section ":ref:`sect_animation_decision`".

Overviews of GSAP features

- Why use GSAP? https://greensock.com/why-gsap/
- Getting started with GSAP https://greensock.com/get-started/
- Writing animation code efficiently https://css-tricks.com/tips-for-writing-animation-code-efficiently/
- Simple animation examples https://greensock.com/learning/

Reference docs

- Getting started with the GSAP docs https://greensock.com/docs/
- Official GSAP docs https://greensock.com/docs/v3/GSAP
- GSAP 3 Release Notes (from November 2019) https://greensock.com/3-release-notes

Informal docs

- GSAP Cheat Sheet https://greensock.com/cheatsheet/
    - lunelson's GSAP CSS properties Cheat Sheet https://gist.github.com/lunelson/7d83ca0c8bdfab170dd3
- GreenSock forums https://greensock.com/forums/

SVG-related

- Animating SVG with GSAP https://greensock.com/svg-tips/

Advanced: Plugins

    Plugins add extra capabilities to GSAP's core. Many plugins are automatically included with GSAP. The `full list of plugins <https://greensock.com/docs/v3/Plugins>`_ includes

    - The `CSSPlugin <https://greensock.com/docs/v3/GSAP/CorePlugins/CSSPlugin>`_ contains the functionality you will most likely use, such as transforms (rotation, scale, x, y), directionalRotation, and autoAlpha)
    - The `AttrPlugin <https://greensock.com/docs/v3/GSAP/CorePlugins/AttrPlugin>`_ changes DOM element attributes (e.g. ``<href>``) and animates any numeric attribute of a DOM element (e.g. width and height) including non-CSS-related attributes
    - The MotionPathPlugin animates any object along a path (or through arbitrary property values)

        - MotionPathPlugin Overview https://greensock.com/motionpath/
        - Plugin docs https://greensock.com/docs/v3/Plugins/MotionPathPlugin
        - MotionPath Showcase https://codepen.io/collection/AyMQkq
        - MotionPath How-To Demos https://codepen.io/collection/DYRzxd
        - MotionPathHelper utility https://greensock.com/docs/v3/Plugins/MotionPathPlugin/MotionPathHelper

    - The `SnapPlugin <https://greensock.com/docs/v3/GSAP/CorePlugins/SnapPlugin>`_ quantises values in an animation (snapping them to a list of values)
    - The `TextPlugin <https://greensock.com/docs/v3/Plugins/TextPlugin>`_ animates text appearing character by character
