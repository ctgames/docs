.. _sect_animations_gsap_timelines:

=============================
Learning about GSAP timelines
=============================

A timeline (see the `GSAP timeline docs <https://greensock.com/docs/v3/GSAP/Timeline>`_) is a sequencing tool that allows one to coordinate different animations (e.g. different calls to :py:func:`gsap.to` and :py:func:`gsap.set`), and allowing absolute and relative control over when each animation starts and finishes.
Animations can be added to a timeline without the programmer needing to modify the timings of any of the existing animations in the timeline.
Modularity is supported by allowing a timeline to be added as an animation within another timeline.


---------------------------------------------------------------
Initialising a timeline (the function :py:func:`gsap.timeline`)
---------------------------------------------------------------

Timelines are initialised with a call to :py:func:`gsap.timeline`.
This function takes a configuration :py:class:`dict` :py:data:`vars` that has the same form as the :py:data:`vars` passed to :ref:`gsap.to() <sect_animations_gsap_func_to>`.
This :py:class:`dict` should contain all the key-value pairs you want the timeline to have (see the `GSAP Timeline vars docs <https://greensock.com/docs/v3/GSAP/Timeline/vars>`_ for more details).
Keys that might be useful for an animation that is repeating while the player plays a game round (e.g. blinking eyes on a character) include `repeat <https://greensock.com/docs/v3/GSAP/Timeline/repeat()>`_, `repeatDelay <https://greensock.com/docs/v3/GSAP/Timeline/repeatDelay()>`_, and `repeatRefresh <https://greensock.com/docs/v3/GSAP/Timeline/vars>`_.

In addition, a key :py:attr:`defaults` is accepted that itself has a value that is a :py:class:`dict`.
Any items in :py:attr:`defaults` will apply to each animation (e.g. each :py:func:`to` and :py:func:`set`) in the timeline.
For more information see the `GSAP Timeline defaults docs <https://greensock.com/docs/v3/GSAP/Timeline#defaults>`_.
The function :py:func:`gsap.timeline` returns an initialised :py:class:`Timeline` object to which :py:func:`to` and :py:func:`set` animations can then be added, as explained in subsequent subsections.

In file :file:`dungeonescape/webapp/__init__.py`, the function :py:func:`update_problem_instance` initialises a timeline, and includes a default :py:attr:`duration` for each animation in the timeline, with

.. code-block:: python

    default_duration = 1
    """A default duration (in seconds) for each step of the animation."""
    # Create a GSAP timeline to hold all of the steps of the animation
    tl = gsap.timeline(
        {
            'defaults': {'duration': default_duration},
            }
        )

In this example, name :py:data:`tl` is used to refer to the initialised :py:class:`Timeline` object.
For all of the examples that follow, you can assume that :py:data:`tl` is an initialised :py:class:`Timeline` object.


-----------------------------------
The function :py:func:`Timeline.to`
-----------------------------------

The function :py:func:`Timeline.to` adds a :ref:`gsap.to() <sect_animations_gsap_func_to>` animation at the end of the current timeline, or elsewhere if the :py:attr:`position` parameter is set.
:py:func:`Timeline.to` takes the same two parameters as :ref:`gsap.to() <sect_animations_gsap_func_to>` (namely, a DOMNode object or list of DOMNode objects, and a dict of attribute-value pairs) plus an additional third argument

    :py:attr:`position`
        A number or :py:class:`str`, default ``'+=0'``: A value that specifies the insertion point of the animation in the timeline as explained in the `GSAP Timeline position parameter article <https://greensock.com/position-parameter>`_.
        If it is a number (or a name referring to an :py:class:`int` or a :py:class:`float`) it means that many seconds from the start of the timeline.
        If it is a string, it is a word in the position parameter mini-language (the full mini-language is not explained here; see the `GSAP Timeline position parameter article <https://greensock.com/position-parameter>`_ for extra features such as labels).

        Examples of this mini-language to specify a position to insert a new animation into the timeline (in all cases, :py:data:`duration` can be an :py:class:`int` or a :py:class:`float`, e.g. 0.25) include

        - ``f'{duration}'`` :py:data:`duration` seconds from the start of the timeline (in such a case :py:data:`duration` could also have been passed as a number without conversion to a string)
        - ``'+=0'`` (the default) at the current end of the timeline
        - ``f'+={duration}'`` :py:data:`duration` seconds past the current end of the timeline (creates a gap)
        - ``f'-={duration}'`` :py:data:`duration` seconds before the current end of the timeline (overlaps)
        - ``'<'`` at the start of the most recently inserted animation, i.e. starting in parallel with the most recently inserted animation
        - ``f'<{duration}'`` :py:data:`duration` seconds after the start of the most recently inserted animation
        - ``f'<-{duration}'`` :py:data:`duration` seconds before the start of the most recently inserted animation
        - ``'<25%'`` 25% after the start of the most recently inserted animation
        - ``'>'`` at the end of the most recently inserted animation (note, not equivalent to the end of timeline word ``'+=0'`` because the most recently inserted animation may end before the end of the timeline)
        - ``f'>{duration}'`` :py:data:`duration` seconds after the end of the most recently inserted animation
        - ``f'>-{duration}'`` :py:data:`duration` seconds before the end of the most recently inserted animation
        - ``'>25%'`` 25% after the end of the most recently inserted animation
        - ``'>-25%'`` 25% before the end of the most recently inserted animation

For more details about this function see the `GSAP Timeline.to() docs <https://greensock.com/docs/v3/GSAP/Timeline/to()>`_.

In file :file:`dungeonescape/webapp/__init__.py`, the function :py:func:`update_problem_instance` adds to the end of the timeline an animation to move an object to the right with

.. code-block:: python

    # Move the robber; increment its 'x' position by `IM_SIDE`
    tl.to(
        robber,
        {'x': f'+={IM_SIDE}', 'duration': duration, 'ease': 'sine.inOut'},
        )

In this example, and in the others from :file:`dungeonescape/webapp/__init__.py`, the :py:attr:`duration` key had been in the timeline defaults, but is included in these code examples for transparency.

In file :file:`dungeonescape/webapp/__init__.py`, the function :py:func:`update_problem_instance` adds to the end of the timeline an effect where one object fades in at the same time as another object fades out.
It does this by scheduling two animations to occur simultaneously, by passing ``'<'`` as the optional position parameter to the second animation, with

.. code-block:: python

    # Hide the key on the ground to illustrate it being picked up by the robber
    tl.to(
        f'#dungeon_{dungeon_index}',
        {'duration': duration, 'autoAlpha': 0},
        )
    # At the same time show the key in the bag
    tl.to(
        f'#key_{key_index}',
        {'duration': duration, 'autoAlpha': 1},
        '<',
        )

In file :file:`dungeonescape/webapp/__init__.py`, the function :py:func:`update_problem_instance` adds to the end of the timeline three animations in the following way.
The first runs for :py:data:`duration` seconds and the other two animations run in sequence with each other, but in parallel with the first (i.e. the first runs in parallel with the second, but halfway through the first the second finishes and the third starts).
This is achieved with

.. code-block:: python

    # Remove the top key from the bag
    tl.to(
        f'#key_{key_index}',
        {'duration': duration, 'autoAlpha': 0},
        )

    # At the same time, hide the door lock for half the duration
    tl.to(
        f'#dungeon_{dungeon_index}',
        {'autoAlpha': 0, 'duration': duration / 2},
        '<',
        )

    # After the door lock has been hidden, but half-way through
    # the key being removed, swing the door open.
    tl.to(
        f'#dungeon_{dungeon_index}_door',
        {'scaleX': 0.2, 'duration': duration / 2},
        '>',
        )

This is a good example for the usefulness of the ``'>'`` word to add an animation at the end of the previously inserted animation, as distinct from adding the animation at the end of the timeline.


------------------------------------
The function :py:func:`Timeline.set`
------------------------------------

The function :py:func:`Timeline.set` instantly changes the specified attributes of DOMNode objects at the end of the current timeline, or elsewhere if the :py:attr:`position` parameter is set.
:py:func:`Timeline.set` takes the same three parameters as :py:func:`Timeline.to`.
More details can be found in the `GSAP Timeline.set() docs <https://greensock.com/docs/v3/GSAP/Timeline/set()>`_.)

In file :file:`dungeonescape/webapp/__init__.py`, the function :py:func:`update_problem_instance` adds to the end of the timeline a command to instantly hide a particular SVG image element with

.. code-block:: python

    # Hide the door after it has been opened
    tl.set(
        f'#dungeon_{dungeon_index}_door',
        {'autoAlpha': 0},
        )


Changing non-CSS-related attributes in a timeline using the :py:attr:`attr` key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Although :py:func:`Timeline.set` is typically associated with setting those attributes of a DOMNode object that are typically animated (such as position, rotation, scale, opacity, and so on) it can also set attributes that are not associated with animation using the :py:attr:`attr` key.
In fact, between directly referencing CSS-related attributes as shown above and using :py:attr:`attr` to reference non-CSS-related attributes, any attribute of a DOMNode object that can be changed can be changed as part of a GSAP timeline.
For example, this code snippit changes the ``<href>`` of a DOMNode element with id :py:data:`id` in a timeline with name :py:data:`tl`

.. code-block:: python

    tl.set(
        f'#{id}',
        {'attr': {'href': IM_PATH.format('cat')}},
        )
