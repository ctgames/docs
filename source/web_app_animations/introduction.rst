.. _chap_web_app_animations:

.. centered:: :title:`Chapter: Animations for web app games`

==================================
Introduction to web app animations
==================================

After a web app version of a game has been developed, it is appropriate to look at what simple animations can be included to make the game more engaging for children.
This chapter explains how to conveniently introduce basic 2D animation of SVG images into CTGames web app games.
The preferred approach is to use the `GreenSock GSAP JavaScript animation library <https://greensock.com/gsap/>`_.

.. seealso::
    Chapter ":ref:`Web app games <chap_web_app_games>`" tells you everything you need to know to develop the web app version of a CTGames game.
    This must be completed before introducing animations.

.. seealso::
    For a list of external websites that you can use to learn more about GSAP (including the official GSAP docs), see section ":ref:`sect_learning_gsap`".

.. seealso::
    For an analysis of the different web app animation technologies considered for CTGames, see section ":ref:`sect_animation_decision`".

..
    **HIDDEN**
    .. seealso::
    Direct manipulation and animation of a SVG image (without any animation library) is possible in a web app.
    An example is given in section ":ref:`sect_animations_direct`".


The file that needs to be modified to add an animation to a web app version of a game (where :file:`x` is the directory name of the game) is

    - :file:`x/webapp/__init__.py`, where the web app front-end of your game was defined.
