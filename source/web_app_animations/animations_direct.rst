.. _sect_animations_direct:

==================================
Direct manipulation of a SVG image
==================================

Direct manipulation of SVGs is unlikely to ever be the preferred option while a convenient animation library such as :ref:`GSAP <sect_learning_gsap>` is available.
However, for historical purposes we present an example of directly modifying the attributes of a SVG.
This is not an example of animation, rather it is an example of the low-level DOM manipulation that would be possible with Brython (without any JavaScript library) if desired.

In file :file:`password/webapp/__init__.py`, the function :py:func:`format_problem_instance` inserts a SVG element of a file :file:`pig.svg` into a SVG instance in the DOM.
While creating the SVG element, a special parameter ``inline=True`` is passed with

.. code-block:: python

    pet_name = 'pig'
    fname = IM_PATH.format(pet_name)
    # Some code hidden for this example
    svg_instance <= create_image_from_file(
        id=pet_name, x=x, y=0, width=200, height=200, href=fname, inline=True
        )

    return TextImageSeq(dom_element_seq=[svg_instance])

This parameter allows the internals of the SVG to be exposed to manipulation (using Brython, in our case).
In file :file:`password/webapp/__init__.py`, the function :py:func:`modify_answered_problem_instance` specifies that when a player submits their answer the character's ears should change colour and that the head should rotate (clockwise if the player was correct, counter-clockwise otherwise) with

.. code-block:: python

    def modify_answered_problem_instance() -> None:
        """Docstring hidden for this example."""
        # Modify selected parts of the character SVG. These parts of the SVG
        # will have been manually given ids using Inkscape.
        document['pig_ear_left'].style.fill = '#ff9999'
        document['pig_ear_right'].style.fill = '#ff9999'

        pig_head = document['pig_head']
        if game_state.player_correct:
            # Rotate -10 deg. (i.e. 10 deg. clockwise) about centre of element
            pig_head.setAttribute(
                'transform',
                'matrix(0.98480775,0.16612754,-0.18150928,0.98480775,'
                '7.2962881,73.884641)',
                )
        else:
            # Rotate +10 deg. about centre of element
            pig_head.setAttribute(
                'transform',
                'matrix(0.98480775,-0.16612754,0.18150928,0.98480775,'
                '-20.596189,-71.550053)',
                )

This works because the SVG file itself has been modified to include :attr:`id` attributes for the SVG elements that we wish to animate, specifically, the SVG element representing the head in the .svg file has the form
::

    <g
        id="pig_head"
        ...
        <path ... id="pig_ear_left" ... />
        <path ... id="pig_ear_right" ... />
        ...
    </g>

Incorporating animation at this low level typically involves calls to the JavaScript function :func:`requestAnimationFrame` (which can be used to animate the attributes of all DOM elements, not only SVG image attributes). Several CTGames web apps use :func:`requestAnimationFrame` for animation. Also, several CTGames web apps use `Snap.svg <http://snapsvg.io/>`_. Both approaches are being phased out in favour of GSAP (which itself uses :func:`requestAnimationFrame` under the hood) and so only GSAP is covered in these docs.
