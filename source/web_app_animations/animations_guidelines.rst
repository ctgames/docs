.. _sect_animations_guidelines:

==========================================
Guidelines for animations in web app games
==========================================

This section gives some guidelines on how to embellish one's fully-functional and graphical CTGames web app game with animations.

There is only one subsection so far

    ":ref:`sect_animations_construct_logic`" that explains why, for programming-style CTGames games, the animation should be calculated in the :file:`logic.py` file, and gives examples.


.. _sect_animations_construct_logic:

---------------------------------------------
Constructing an animation in :file:`logic.py`
---------------------------------------------

Some CTGames games are programming-style games, which have the property that there is more than one correct answer (potentially an unbounded amount of answers) and that it is only at the player answer submission stage (when the player's answer is checked for correctness) that an appropriate animation can be constructed.
Our experience of coding programming-style games with the CTGames framework leads us to recommend that one should construct the logic of the animation (i.e. a list of bespoke :py:class:`str` keywords encoding the animation steps) in :file:`logic.py` side-by-side with the code that tests the correctness of the player's answer.

.. tip:: CTGames games that use :file:`logic.py` to compute whether the player's answer is correct or not, and then use a separate function in :file:`webapp/__init__.py` to construct an animation to show why the player's answer is correct/incorrect, are effectively repeating the same computation. This is inefficient and runs the risk (for complicated games) of a hard-to-find bug in one of the functions causing the two functions to disagree (e.g. the :file:`logic.py` file says the player's answer is wrong but the :file:`webapp/__init__.py` animation fails to demonstrate this).

In file :file:`dungeonescape/logic.py`, the function :py:func:`process_input_and_solve_task` constructs an animation sequence, consisting of a list of ``'push'`` and ``'pop'`` strings, with

.. code-block:: python

    # Note: this code has simplified from that in the game for explanatory purposes

    OPEN_BRACKETS = ('[', '{', '(', '<'}
    """The mapping from open brackets to appropriate closing brackets."""

    MATCHING_BRACKETS = {']': '[', '}': '{', ')': '(', '>': '<'}
    """Inverse of the mapping (closing brackets to opening brackets)."""

    stack = []
    """The keys in the stack (may contain duplicates)."""

    # Building up the animation based on player's selected option
    animation = []
    for key_type in players_answer:
        if key_type in OPEN_BRACKETS:
            stack.append(key_type)
            animation.append('push')
        else:
            if stack and stack.pop() == MATCHING_BRACKETS[key_type]:
                animation.append('pop')
            else:
                break

The code determines whether name :py:data:`players_answer` contains appropriately matching brackets. At the point that matching brackets are not found, the animation sequence finishes. Properties of the animation sequence (including its length) can be used to determine whether the player's answer is correct, and so this is an example of constructing the animation sequence side-by-side with checking the player's answer.
