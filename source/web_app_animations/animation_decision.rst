.. _sect_animation_decision:

=====================================
Choice of web app animation framework
=====================================

We use `SVG <https://www.w3.org/TR/SVG/>`_ images for graphics in CTGames because they are scalable for multiple screen resolutions (phones, tablets, and computer screens) and also for high-resolution printing (posters and workbooks of computational thinking puzzles using the graphics directly from CTGames games).

We use the `Greensock GSAP JavaScript animation library <https://greensock.com/gsap/>`_ to animate SVG images in the CTGames framework. Deciding on GSAP was not a straightforward process. This section explains the reasoning behind choosing GSAP.

There are tens if not hundreds of different ways to animate objects in the DOM through JavaScript and :abbr:`CSS (cascading style sheets)`, all of which can be accessed using Brython. One of the reasons for writing this section is to remember the reasons behind our decision should we decide to revisit the choice of animation library again.

.. seealso:: For examples of using the web app animation technologies covered in this section, see section ":ref:`sect_animations_basic`".


--------------------------------------------------
Requirements for the CTGames SVG animation library
--------------------------------------------------

Our requirements for animation are two fold. First, we want to be able to move/rotate/modify SVG elements in the web app, and secondly we need a way to dynamically construct timelines.

Timelines offer the capability to dynamically construct sequences of animation steps where:

#. the steps are not known in advance; they are specified by a CTGames player while playing a CTGames game (e.g. a game that asks the player to program a robot using a simple programming language),

#. steps can be specified to be performed in sequence (when one ends, the next one automatically starts, without having to hard-code complicated timing delays/durations), and

#. steps can be specified to be performed in parallel.

Beyond these requirements, we have a strong preference for a free and open source animation library, rather than a proprietary/commercial one, should one be found that offers:

a. comparable reliability and ease of use as other libraries

b. has a large user base and/or multiple developers

c. fixes bugs at least on an annual basis



----------------------
Technologies available
----------------------

The different technologies we investigated are summarised in this section.



.. _sect_animation_requestanimationframe:

Direct manipulation of attributes and :func:`requestAnimationFrame`
-------------------------------------------------------------------

SVG images have `attributes <https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute>`_ such as position-related attributes (:attr:`x`, :attr:`y`), dimensions-related attributes (:attr:`scale`, :attr:`width`, :attr:`height`), :attr:`opacity`, :attr:`transform`, and so on. The :attr:`transform` attribute of SVG elements allows various `transform functions <https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/transform>`_ such as rotate, translate, skew, and scale. Official docs for both `SVG 1.1 transforms <https://www.w3.org/TR/SVG11/coords.html#TransformAttribute>`_ and `SVG 2.0 transforms <https://www.w3.org/TR/css-transforms-1/#svg-transform>`_ are available to learn more about the SVG :attr:`transform` attribute, as well as `stakeholder tutorials <https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Basic_Transformations>`_ and `in-depth blog posts <https://css-tricks.com/transforms-on-svg-elements/>`_.

Changing attribute values and transform function parameters in a SVG image have an immediate effect. However, by incrementally changing the values while calling the JavaScript function :func:`requestAnimationFrame` one can have full control of an animation effect.

.. seealso::
    Documentation for Animation Frames and :func:`requestAnimationFrame` includes

    - the `official docs <https://html.spec.whatwg.org/multipage/imagebitmap-and-animations.html#animation-frames>`_,

    - the `previous version of the official docs <https://www.w3.org/TR/animation-timing/#dom-windowanimationtiming-requestanimationframe>`_,

    - `stakeholder docs <https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame>`_, and

    - calling :func:`requestAnimationFrame` `from Brython <https://brython.info/static_doc/en/timer.html>`_.



.. _sect_animation_nativesvg:

Native SVG animations (SMIL and SVG 2.0)
----------------------------------------

A `complicated mixture <https://en.wikipedia.org/wiki/SVG_animation>`_ of approaches exists to `animate SVG objects <https://www.w3.org/TR/SVG/animate.html>`_ in browsers. The most basic approach is to use `SVG elements <https://developer.mozilla.org/en-US/docs/Web/SVG/Element>`_ such as the `<animate> <https://developer.mozilla.org/en-US/docs/Web/SVG/Element/animate>`_ element, for example with its :attr:`from` and :attr:`to` attributes as in ``<animate attributeType="XML" attributeName="x" from="0" to="100" dur="3s"/>``.

The built-in `animation features in SVG 1.1 <https://www.w3.org/TR/SVG11/animate.html>`_ are defined in terms of a separate standard called :abbr:`SMIL (Synchronized Multimedia Integration Language)` (see `SMIL 3.0 Animation <https://www.w3.org/TR/SMIL/smil-animation.html>`_) which is widely supported by all browsers.

.. seealso:: A highly-recommended huge tutorial on SVG animations is available: `A Guide to SVG Animations (SMIL) <https://css-tricks.com/guide-svg-animations-smil/>`_ by Sara Soueidan, 2018.

The latest version of SVG (`SVG 2.0 <https://www.w3.org/TR/SVG2/>`_) proposes to subsume and extend the functionality of SMIL, so even if the future of SMIL as a separate standard is uncertain, the exact functionality is likely to live on within the SVG standard. See the latest proposed `SVG Animations <https://svgwg.org/specs/animations/>`_ detailing its `extensions to SMIL <https://svgwg.org/specs/animations/#RelationshipToSMILAnimation>`_.

However, while Chrome's browser engine Blink supports SVG 1.1, it has been slow to implement SVG 2.0 compared with others (specifically, Firefox's Gecko). For example, the `SVG 2.0 standard <https://www.w3.org/TR/SVG2/>`_ states that ``<svg>`` elements should accept transform attributes, and while Firefox has implemented this for several years, as of January 2022 Chrome still does not. (This and other examples have caused headaches with CTGames development where the default development browser is Firefox.) This means that the animation functionality in SVG 2.0 is not likely to be generally available outside Firefox anytime soon. Furthermore, the Chrome development team announced that it was looking to `deprecate SMIL <https://groups.google.com/a/chromium.org/g/blink-dev/c/5o0yiO440LM>`_ and while it then `paused the deprecation process <https://groups.google.com/a/chromium.org/g/blink-dev/c/5o0yiO440LM/m/YGEJBsjUAwAJ>`_ the Chrome developers have made it clear that supporting SMIL separately is not in their long-term plans.

So, if CTGames is to support Chrome in both the short-term and long-term it seems that the built-in animation capabilities for SVG (whether in the form of SMIL or SVG 2.0) are not wise options for our limited CTGames development time budget.



.. _sect_animation_webanimations:

Web Animations API
------------------

Something that Chrome is willing to support is the `Web Animations API <https://www.w3.org/TR/web-animations-1/>`_, a proposal to combine several existing SVG animation approaches into one framework, that is also well-supported by other browsers. Stakeholder `docs <https://developer.mozilla.org/en-US/docs/Web/API/Web_Animations_API>`_ and `tutorials <https://developer.mozilla.org/en-US/docs/Web/API/Web_Animations_API/Using_the_Web_Animations_API>`_ are available.

However, the Web Animations API is quite low level and would require a high-level library to be written using Brython to make it convenient to use for timelines, for example. Although the `official docs <https://www.w3.org/TR/web-animations-1/#timelines>`_ make it look full-featured, the limited examples in `stakeholder docs <https://developer.mozilla.org/en-US/docs/Web/API/DocumentTimeline>`_ and `online tutorials <https://danielcwilson.com/blog/2015/07/animations-part-2/>`_ make it seem rather limited.



CSS animations
--------------

CSS animations are nice and simple, but you cannot specify that one animation should start when another one ends. Instead you have to manually hardcode durations/delays to sync multiple animations.



.. _sect_animation_jslibrary:

JavaScript animation library
----------------------------

Several free and open source SVG animation libraries exist, but we were worried about tying ourselves to a JavaScript library that may not be actively developed in the future if it did not have a large user base. Commercial options exist also, but for CTGames a paid subscription model is uneconomical as we often have a half year and longer breaks in new games development.

So we tried special purpose libraries that would smooth over the uneven SVG support among browser implementations (reduces the need to test on multiple browsers and operating systems -- for example Chrome currently (2021) uses WebKit on iOS and Blink on all other operating systems).

- Snap.svg
    `Snap.svg <http://snapsvg.io/>`_ is the successor to the well-known `Raphaël JavaScript library <https://dmitrybaranovskiy.github.io/raphael/>`_ that still has a very large user base. The author of Raphaël discontinued it to instead create Snap.svg as a more modern equivalent. Unfortunately, Snap.svg has some well-publicised bugs that limit its functionality. The author has discontinued work on Snap.svg, with the latest version being v0.5.1 from February 2017, so it is unlikely these bugs will get fixed.

- SVG.js
    `SVG.js <https://svgjs.dev/>`_ is faster than Snap.svg and contains all of the functionality needed for CTGames, including timelines. It has good documentation, a bunch of `CSS-Tricks articles <https://css-tricks.com/search-results/?q=svgjs>`_, and a large user base. It is actively maintained with regular releases and 9.5k GitHub stars, and its own tag with 500+ tagged questions on Stack Overflow (all as of early 2022).

- Velocity
    The `Velocity <http://velocityjs.org/>`_ JavaScript animation library is a very high quality and extensive library that looks like it has everything needed for CTGames. However, the docs seem to be written for someone coming from jQuery (jQuery is mentioned 81 times on the first page of the website --- tested 5 March 2022). The Python developers of CTGames are using Brython because they don't want to  have to learn JavaScript just to make a web app, and it seems that learning about jQuery is a prerequisite in order to use Velocity.

- GSAP
    `Greensock GSAP <https://greensock.com/gsap/>`_ is a very polished JavaScript animation library that claims to always use the most efficient JavaScript solution under the hood, and to smooth over (SVG, and other) specification implementation differences between browsers. The documentation is extensive, online tutorials are numerous, and the user base seems very large (1,600+ tagged questions on Stack Overflow, and Greensock claims GSAP is used by 8.5 million websites). The basic version of GSAP (which is sufficient for CTGames) is `open source <https://github.com/greensock/GSAP>`_ software, however it uses a licence that restricts it to non-commercial projects -- a paid version of GSAP is needed to build commercial products.




Libraries with timeline support
-------------------------------

We need timeline functionality to allow the CTGames games programmers to conveniently program animations for their games. Options include:

- writing our own timeline library using the JavaScript function :func:`requestAnimationFrame` or the equivalent function in the Web Animations API; for example, a function for translation would have to be written that takes a SVG image and translation parameters as arguments and repeatedly updates the values in the SVG's attributes by some small increment while calling :func:`requestAnimationFrame` each time for a smooth animation,

- the Web Animations API has full-featured but low-level `timeline support <https://www.w3.org/TR/web-animations-1/#timelines>`_ through :class:`DocumentTimeline` objects,

- the `Snapsvg Animation Queue <https://github.com/mystroken/snapsvg-animation-queue>`_ provides timeline support for `Snap.svg <http://snapsvg.io/>`_,

- `Tweene <https://tweene.com/>`_ provides timeline support for the `Velocity <http://velocityjs.org/>`_ JavaScript library, and

- use an all-in-one solution of an animation library that includes timeline functionality, for example `SVG.js <https://svgjs.dev/>`_ and `GSAP <https://greensock.com/gsap/>`_ provide their own high-level timeline support.



Summary
-------

In summary:

- Animating with CSS
    --- does not offer full control of animations

- Animating with SVG 2.0 or SMIL
    --- offers full control from Brython, but Chrome is slow to adopt the former and may drop support for the latter

- Animating with the Web Animations API
    --- offers full control from Brython, but low-level (will require us to write higher-level Brython wrapper functions)

- Animating with JavaScript (using :func:`requestAnimationFrame`)
    --- offers full control from Brython, but low-level (will require us to write our own timeline library, for example)

- Animating with JavaScript (using a library such as Snap.svg, SVG.js, GSAP, Velocity)
    --- offers full control from Brython, a huge number of options from low-level to high-level, but if the library is ever discontinued we will have to re-do all animations


--------
Analysis
--------

Our options for a long-lived (supported by all browsers in the long-term) animation API for CTGames developers seem to be:

#. rewrite the CTGames SVG animation API to use Web Animations functions on SVG images instead of using :func:`requestAnimationFrame` as it does currently

#. use an off-the-shelf high-level SVG animation library (free and open source or proprietary/commercial)

#. ignore short-term compatibility, build an CTGames animation API over native SVG animations that works exclusively on Firefox for now, and wait for Chrome to catch up with its implementation of SVG 2.0

#. none of the above; decide not to provide any animation API and force CTGames developers to learn the low-level Web Animations API


------------
Our decision
------------

The initial animation approach we took in CTGames was to animate objects by directly modifying their properties and attributes and calling the JavaScript function :func:`requestAnimationFrame` that works with SVGs as well as all other DOM elements (see section ":ref:`sect_animation_requestanimationframe`" above). We programmed a high-level Brython API on top of the low-level JavaScript and SVG animation functions to allow CTGames developers to conveniently build animations without needing to learn about the underlying JavaScript and SVG machinery. This fully-featured library in CTGames also provided sequential and parallel timelines.

However, variable SVG support across different browsers (details given above) meant that, over time, particular animations broke with particular browsers. Although the original plan had been to periodically update the library in response to these kinds of browser vendor changes, while still keeping the library's API constant, ultimately this plan became too challenging; features broke at a faster rate than we could cope with given the limited development time available to us.

The built-in animation functionality native to SVG images initially looked like the best alternative option for CTGames. However, as described in section ":ref:`sect_animation_nativesvg`", the uneven support among browsers makes this a headache.

We felt we had to move to an established animation library that automatically dealt with inhomogeneities across browsers. A selection of off-the-shelf JavaScript libraries is outlined in section ":ref:`sect_animation_jslibrary`" (e.g. Snap.svg, SVG.js, GASP, Velocity). We first adopted `SNAP.svg <http://snapsvg.io/>`_ but after after a couple of years (due to the lack of updates, as explained in section ":ref:`sect_animation_jslibrary`") we needed to change. Our choice was between `SVG.js <https://svgjs.dev/>`_ and `GSAP <https://greensock.com/gsap/>`_. We went with the latter because the documentation was so extensive and polished (how-to videos, etc.) that it would be more appreciated by the relatively inexperienced undergraduate programmers working on CTGames. If the open source but non-free (non-libre) licence of GSAP ever becomes a problem we will switch to SVG.js.
