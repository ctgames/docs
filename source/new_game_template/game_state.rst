===============================
Game state in the template game
===============================

The two subsections in this section are

    section ":ref:`sect_modify_gamestatecustom`", and
    section ":ref:`sect_template_modify_create_game_round`".


.. _sect_modify_gamestatecustom:

----------------------------------
Modify :py:class:`GameStateCustom`
----------------------------------

(found in :file:`logic.py`)

The :py:class:`GameStateCustom` object is a :py:class:`namedtuple` that is used to store

    - information about the current game round, and
    - information that is specific to how your game works.

These two properties are explained as follows. A lot of information needs to be tracked during a game round, such as what is the game round question, what are the specific game round rules (if any), what are the multiple choice answers for this round, what is the correct answer for this round, what has the player answered, what is the player's current score, and so on. All of these values will be different from round to round, but this type of data is common to every game, so a game state :py:class:`namedtuple` is created in the CTGames framework with specific a field for each of them.

However, each developer's game may have extra information that needs to be stored. Typically, this is information that has been randomly generated specifically for that game round, and will change from one game round to another.

Information that is specific to any particular game would not be appropriate for the CTGames game state :py:class:`namedtuple`, which only has fields for data that is common to every game, e.g. the correct answer(s) for the current game round. Therefore, a custom :py:class:`namedtuple` :py:class:`GameStateCustom` is available for the game developer to store all of the game state information that is specific to their game.

.. tip:: The design of fields in the :py:class:`GameStateCustom` :py:class:`namedtuple` can go hand-in-hand with the design of the function :py:func:`_decide_on_problem_instance` (see section ":ref:`sect_decide_on_problem_instance`"). As a rule of thumb, whatever your function :py:func:`_decide_on_problem_instance` creates dynamically when making a new round, i.e. anything that will change with different playthroughs, that is not already stored in :py:data:`GAME_BEHAVIOUR`, is likely to be appropriate to be stored in :py:class:`GameStateCustom`.

.. note:: The data in :py:class:`GameStateCustom` is available from all parts of the code of a developer's game. It can be imported into any module, so a developer will have access to it from all places in the code that they may need it, e.g. the file :file:`webapp/__init__.py` that contains the code for the game's web app.

For the template, the only thing that changes from one game round to another is the list of numbers. Therefore, a field for the list of numbers is added to :py:class:`GameStateCustom` with

.. code-block:: python

    GameStateCustom = namedtuple('GameStateCustom', ('custom_field1',))

.. tip:: In the template, this field is given the unhelpful name :py:attr:`custom_field1`. Make sure to change it to a name that is meaningful for your game.

You'll need to ensure that there is a docstring for each field of :py:class:`GameStateCustom`. In the template, there is only one field in the namedtuple, and this looks like

.. code-block:: python

    GameStateCustom.custom_field1.__doc__ = """list: The list of numbers."""

..
    **HIDDEN**
    Examples from CTGames
    ^^^^^^^^^^^^^^^^^^^^^
    << TODO include examples from the framework, e.g. some games put their animations into this >>


.. _sect_template_modify_create_game_round:

-----------------------------------
Modify :py:func:`create_game_round`
-----------------------------------

(found in :file:`logic.py`)

All of the previous steps have concerned data structures. This function is the first function that you have to write.

.. include:: ../command_line_games/include_function_create_game_round.rst
