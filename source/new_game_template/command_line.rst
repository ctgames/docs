===============================================
Command-line functionality in the template game
===============================================


--------------------------
Modify ranges and defaults
--------------------------

(found in :file:`cl.py`)

In the previous section it was explained where the names in ``SublevelBehaviour`` get their values in a game round. There is a second possibility. This is from a teacher specifying a custom round through the web app version of the game, or equivalently, the developer passing parameters for a custom round in the command-line version of the game. It will be important that the input from the teacher is of the correct type and that the value is within the ranges acceptable by your game logic. By putting effort into this step, you will not need to write any code to sanitise inputs and can be sure that your :file:`logic.py` will not raise an exception due to an unexpected input type or value. This is because

    - the web app will ensure the correct type by automatically generating a drop-down box, checkbox, spin button, etc. that only allows the teacher to input values of the correct type, and
    - if the value is the correct type, but an incorrect value, the framework will automatically choose the closest valid value.

There are two objects to modify in this step, ``ROUND_BEHAVIOUR_RANGES`` and ``DEFAULT_ROUND_BEHAVIOUR``.


.. _sect_round_behaviour_ranges:

Modify ``ROUND_BEHAVIOUR_RANGES``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``ROUND_BEHAVIOUR_RANGES`` object specifies the type and allowed values for each parameter of ``SublevelBehaviour``. In the template, it is specified as:

.. code-block:: python

    ROUND_BEHAVIOUR_RANGES: SublevelBehaviour = SublevelBehaviour(
        param1=range(2, 20),
        param2=range(1, 7),
        )

The names of the parameters must match the names of your ``SublevelBehaviour``. You must assign an object to each parameter. Your choices of object are

    - a standard Python type, e.g. ``param1=int`` or ``param1=bool``
    - an range object, e.g. ``param1=range(2, 11)``
    - an enumerated type from CTGames, e.g. ``param1=ReplayIncorrectRound`` (the game Ordered List uses this)
    - an enumerated type that you define yourself, e.g. ``param1=GameMode`` (see section ":ref:`sect_creating_own_enum`")
    - a namedtuple or class with named attributes, e.g. ``param1=MyClass`` or  ``param1=MyNamedTuple`` (note, if ``MyClass`` is defined in :file:`logic.py` you'll have to import it from there)
    - a sequence object (e.g. a list ``param1=[2, 3, 5, 7, 11, 13, 17]`` or ``param1=MY_LIST``) to restrict the possibilities to a particular set (note, if ``MY_LIST`` is defined in :file:`logic.py` you'll have to import it from there)
    - a relative range object, e.g. ``param2=RelativeRange(ge=3, lt='param1')`` (see section ":ref:`sect_using_relative_range`")


.. _sect_default_round_behaviour:

Modify ``DEFAULT_ROUND_BEHAVIOUR``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``DEFAULT_ROUND_BEHAVIOUR`` object specifies the default value that should be given to this parameter if one is not available. In the template, it is specified as:

.. code-block:: python

    DEFAULT_ROUND_BEHAVIOUR: SublevelBehaviour = SublevelBehaviour(
        param1=5,
        param2=1,
        )

The particular choice of values is up to you, but probably values corresponding to one of the easier levels from your ``SublevelBehaviour`` would be best. You also have the choice of using a relative default object, e.g. ``param2=RelativeDefault('param1')`` here (see section ":ref:`sect_using_relative_default`").

.. note:: CTGames currently implements the following input-sanitising protocol. If the teacher submits a value that is the correct type, but is outside the correct range, the value in the range closest to the submitted value is used. If the teacher submits a value for a parameter that is the incorrect type, then the respective ``DEFAULT_ROUND_BEHAVIOUR`` parameter is used.

..
    Examples from CTGames
    ^^^^^^^^^^^^^^^^^^^^^
    << TODO >>



---------------------------------
Run ``test_follows_framework.py``
---------------------------------

A test suite has been created to check for some of the most common problems that stop a game working with the CTGames framework. To run the test suite, assuming your game is ``catchaball``, in any terminal enter::

    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/ctgames/catchaball/test/
    python test_follows_framework.py

As of 2020, it currently only tests your game's ``SublevelBehaviour``. If it finds any errors, you will need to fix them.
