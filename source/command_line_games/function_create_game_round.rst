.. _sect_function_create_game_round:

=========================================
The function :py:func:`create_game_round`
=========================================

(found in :file:`logic.py`)

This section tells you all you need to know about the function :py:func:`create_game_round`. You will have already read the first subsection below (subsection ":ref:`sect_function_create_game_round_basics`") from Chapter ":ref:`Template game explained <chap_template_game_explained>`" that explained how to get started programming a command-line CTGames game (specifically, subsection ":ref:`sect_template_modify_create_game_round`"). It is reproduced here for completeness.

The full list of subsections in this section is

    - subsection ":ref:`sect_function_create_game_round_basics`" everything you need to get started with this function
    - subsection ":ref:`sect_answer_style_options`" explains the full set of possibilities for the style of answer for one's game (values for key :py:attr:`answer_type` returned from :py:func:`create_game_round`)



.. _sect_function_create_game_round_basics:

------
Basics
------

.. include:: include_function_create_game_round.rst



.. _sect_answer_style_options:

-------------------------------------------------
Answer style options (key :py:attr:`answer_type`)
-------------------------------------------------

The game developer has several possibilities for the style of answer they wish for their game. The choice is made in function :py:func:`create_game_round` in file :file:`logic.py`.

.. note:: When scoring a game round, usually the player gets full marks for a correct answer and (for example for MCQ-type games) a player can lose some marks for an incorrect answer. However, there is also the concept of a partial mark in games. In such a case, a player may get some marks for a good attempt even if their answer is incorrect. In the descriptions below, it is mentioned for which game styles the concept of a partial mark is supported.

The full list of possibilities is:

    ``AnswerType.String``
        The answer to the game round is a string. No marks will be given
        for a partially correct answer (i.e. there is no partial mark).

    ``AnswerType.StringCaseI``
        The answer to the game round is a string. No marks will be given
        for a partially correct answer (i.e. there is no partial mark).
        The comparison is case insensitive.

    ``AnswerType.Partial``
        The answer to the game round is a string. The partial mark will
        be the length of the correct prefix. (Default.)

    ``AnswerType.PartialCaseI``
        The answer to the game round is a string. The partial mark will
        be the length of the correct prefix. The comparison is case
        insensitive.

    ``AnswerType.MCQ``
        The answer to the game round is a label for one of the multiple
        answers presented to the player (it was a multiple-choice
        question). Negative marking is used for the partial mark.

    ``AnswerType.ImgMCQ``
        The same as ``MCQ`` but allows images to be displayed instead of
        text in the Brython version of the game.

    ``AnswerType.Int``
        The answer to the game round is an int. No marks will be given
        for an incorrect answer.

    ``AnswerType.NaturalMin``
        The answer to the game round is a non-negative int. The player's
        answer will be used as the partial mark. An answer greater than
        or equal to the target is deemed correct. If there is a goal,
        and that goal is achieved, the partial mark will be doubled.

    ``AnswerType.NaturalMax``
        The answer to the game round is a non-negative int. An answer
        less than or equal to the target is deemed correct. If correct,
        the target minus the player's answer will be used as the partial
        mark. If there is a goal, and that goal is achieved, the partial
        mark will be doubled.

    ``AnswerType.Bag``
        The answer to the game round is a bag (an unordered list, or,
        equivalently, a set that can contain multiple elements). The
        number of elements in the set will be used as the partial mark.
        No marks will be given for an incorrect answer. It is assumed
        that the bag contains strings (as that is what comes naturally
        from the command line) and that each string contains no spaces
        and commas.

    ``AnswerType.BagCaseI``
        As with ``Bag`` above, except the strings are compared case-insensitively.


Examples
^^^^^^^^

In file :file:`countfromzero/logic.py`, the function :py:func:`create_game_round` specifies that the answer type should be an integer, with

.. code-block:: python

    updates = {
        'answer_type': AnswerType.Int,
        'targets': [target],  # `targets` is a list of correct answers
        'custom': custom,
        }

    return updates


In file :file:`justaddition/logic.py`, the function :py:func:`create_game_round` specifies that the answer type should be a multiple-choice question label (e.g. 'A', 'B', 'C', ...) with

.. code-block:: python

    updates = {
        'answer_type': AnswerType.MCQ,
        'multiple_answers': multiple_answers,
        'targets': targets,  # `targets` is a list of correct answers
        'rules_text': mcq_rules_text(multiple_answers),
        'custom': custom,
        }

    return updates


In file :file:`justreverse/logic.py`, the function :py:func:`create_game_round` specifies that the answer type should be a case-insensitive string where the player gets a partial mark depending on the position of the first incorrect character in the string (full marks if there is no incorrect character) with

.. code-block:: python

    updates = {
        'answer_type': AnswerType.PartialCaseI,
        'targets': [target],  # `targets` is a list of correct answers
        'target_item_name': _TARGET_ITEM_NAME,
        'custom': custom,
        }

    return updates


In file :file:`justodd/logic.py`, the function :py:func:`create_game_round` specifies that the answer type should be a set of strings, with

.. code-block:: python

    updates = {
        'answer_type': AnswerType.Bag,
        'targets': [target],  # `targets` is a list of correct answers
        'custom': custom,
        }

    return updates


..
    << TODO: Give a full list of all possible update keys, by looking at
    next_game_round() in common.py. >>

..
    TODO: The full list of supported keys in the :py:class:`dict` returned are:
