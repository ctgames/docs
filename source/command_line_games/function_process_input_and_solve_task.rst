.. _sect_function_process_input_and_solve_task:

====================================================
The function :py:func:`process_input_and_solve_task`
====================================================

(optional in :file:`logic.py`)

The function :py:func:`process_input_and_solve_task` is used in CTGames games where we need to process the player's input to determine if it is correct or not (for games where there are many possible correct solutions, e.g. programming style games) and/or where we need to compute the correct answer.

Many games do not need to define this function. For example, in a multiple-choice-based game, the correct answer will have been determined at the same time as the multiple answers were decided, so defining this function might not be necessary.

The function takes as arguments :py:attr:`game_state`, :py:attr:`player_input`, and :py:attr:`animate`, and returns an object of type :py:class:`ProcessedInput`.

TODO: explain these arguments and class ProcessedInput.

Dungeon Escape is an example of a game that uses :py:func:`process_input_and_solve_task`.
