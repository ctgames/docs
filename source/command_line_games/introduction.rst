.. _chap_command_line_games:

.. centered:: Chapter: Command-line games

==================================
Introduction to command-line games
==================================

This chapter documents the programming interface for the CTGames framework with respect to command-line games.

.. seealso::

    Chapter ":ref:`Game development <chap_game_development>`" provides general guidelines for the development of CTGames games.

    Chapter ":ref:`Template game explained <chap_template_game_explained>`" explains how to get started programming a command-line CTGames game.

    Chapter ":ref:`Web app games <chap_web_app_games>`" provides guidelines for how to turn a CTGames command-line game into a web app.

The files that need to be modified to make a command-line game are

    - :file:`logic.py` is where the game logic and all the data structures specific to your game are defined,
    - :file:`behaviour.py` is where the number of rounds in the game is defined and how the difficulty and game mechanics change from round to round,
    - :file:`text_constants.py` is where all of the static human-readable strings in the game are defined (e.g. question, instructions), and
    - :file:`cl.py` is where the type and allowed values for the parameters that control game difficulty and game mechanics are defined, as well as any dynamic formatting of the human-readable strings in the game.


-----------------------
Overview of the chapter
-----------------------


General guidelines

    The section ":ref:`sect_guidelines_for_command_line_development`" gives general guidelines for developing command-line CTGames games.


Specific functions

    The section ":ref:`sect_function_create_game_round`" explains the details of modifying the function :py:func:`create_game_round`.

    The section ":ref:`sect_function_process_input_and_solve_task`" explains the details of modifying the function :py:func:`process_input_and_solve_task`.


Advanced functionality

    Section ":ref:`sect_advanced_functionality_in_logic_py`" describes advanced functionality available through the :file:`logic.py` file for each game that you may need as your game becomes more sophisticated.

    Similarly, section ":ref:`sect_advanced_functionality_in_cl_py`" describes advanced functionality available through the :file:`cl.py` file.


Checklist for game completion

    Section ":ref:`sect_checklist_for_game_completion`" reminds you of all of the tasks that must be completed in order to finish developing a command-line game.
