Function :py:func:`create_game_round` creates a single round of one's game dynamically.
It acts as the "main" method of the game for the game developer.
There is no actual logic in this function. Instead, it is where data structures returned from the game logic are used to populate a :py:class:`dict` to be returned to the CTGames framework.

.. note::
    The CTGames framework calls this function each time a new game round is to be created.
    The framework passes a :py:class:`namedtuple` of type :py:class:`SublevelBehaviour`.
    It either chooses the appropriate :py:class:`namedtuple` from file :file:`behaviour.py` or, if it is a custom round, from the developer through the command line or from a teacher through the web app).
    The game developer does not need to understand how the framework chooses which :py:class:`SublevelBehaviour` object to pass as :py:attr:`round_behaviour`, they just need to be able to write this function so that it responds appropriately whatever values are passed.

.. tip::
    The game developer can be confident that the only values that will be passed in parameter :py:attr:`round_behaviour` will be those allowed by :py:data:`ROUND_BEHAVIOUR_RANGES` (see section ":ref:`sect_round_behaviour_ranges`").
    Therefore, it is unlikely that there will be a need for the developer to check for invalid values.

.. note::
    The :py:class:`dict` returned from this function is used by the CTGames framework to update the game state that it maintains throughout the game round.
    The :py:class:`dict` should contain everything that the CTGames framework needs for a new round of your game.

There are five aspects of this function to update, each explained in detail in the subsequent sections:

    i. Decide on an answer type
    ii. Use the game logic to decide on a game instance
    iii. (Only for MCQ-type games) Decide on a suitable list of multiple choice answers
    iv. Populate a new :py:class:`GameStateCustom` object
    v. Construct a :py:class:`dict` to update the game state


Decide on an answer type
^^^^^^^^^^^^^^^^^^^^^^^^

You must decide what is the style of answer expected from the player for your game, e.g ``AnswerType.Int`` if the game expects an integer, ``AnswerType.MCQ`` if it is an MCQ game, and so on.

.. note:: The choice made here affects what the player will be presented with during a game round (e.g. MCQ answers, a text box, an integer spin button, and so on) and how the player's answer will be pre-processed (all values entered on the command line are strings; if you specify ``AnswerType.Int`` for your game, the CTGames framework will ensure those strings can be converted into integers before passing them to your code).

The full list of possibilities for the style of answer is given in subsection ":ref:`sect_answer_style_options`".

When you decide on the style of answer for your game, you will enter your decision into the :py:class:`dict` that updates the game state (explained later in this section).


Use the game logic to decide on a game instance
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A call must be made to the function :py:func:`_decide_on_problem_instance` passing a value for :py:attr:`round_behaviour` to it.
This function (which you can treat as a black box just for now -- we'll fill in the code for it later) has all of the game logic for the game.
It should return

    - whatever data should be stored in :py:class:`GameStateCustom`, which is whatever information is unique to this game round (and should have the same name(s) as the field(s) in :py:class:`GameStateCustom`), and

    - the correct answer (or sequence of correct answers, if there are multiple equally correct ways the player can answer).

In Game Template Int, this line looks like

.. code-block:: python

    custom_field1, target = _decide_on_problem_instance(round_behaviour)

In Game Template MCQ, this line looks like

.. code-block:: python

    custom_field1, target_long = _decide_on_problem_instance(round_behaviour)

.. note::
    There is a slight difference between the two names we use for the correct answer: in MCQ-type games we use the name :py:data:`target_long`, and for all other types of game we use :py:data:`target`.
    This is a convention to acknowledge that there are two concepts of correct answer in MCQ-type games.

        Firstly, there is the logical answer to the game round (i.e. the correct answer to the logical problem, which might be an :py:class:`int` such as ``5``, a :py:class:`bool` such as ``True``, or a :py:class:`str` such as ``'0011'``), and

        secondly, there is the answer that we want the player to select in a MCQ-type game, which is always one of the multiple-choice question labels (``'A'``, ``'B'``, ``'C'``, and so on).

    In MCQ-type games, we always use :py:data:`target_long` for the logical answer and :py:data:`target` for the correct multiple-choice question label.
    Further, in MCQ-type games as well as all other games, :py:data:`target` will refer to the exact string that the player is supposed to enter.


Decide on a suitable list of multiple choice answers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is only relevant for MCQ-type games. This function takes as parameters whatever is needed for that particular game to return

    - a randomised-order list of possible answers, one of which must be name :py:data:`target_long` (the logical answer to the game round), and

    - the multiple-choice question label ('A', 'B', 'C', and so on) that represents the location of :py:data:``target_long` in the list.

In Game Template MCQ, what is needed as parameters are the logical answer to the game round and the list of values given in the question, and this looks like:

.. code-block:: python

    multiple_answers, targets = _decide_on_answers(target_long, custom_field1)

..
    << TODO: other games' examples? >>


Populate a new :py:class:`GameStateCustom` object
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The custom information returned from :py:func:`_decide_on_problem_instance` needs to be put in the correct format (a :py:class:`GameStateCustom` object).
In the template, this looks like:

.. code-block:: python

    custom = GameStateCustom(custom_field1)

..
    << TODO: other games' examples? >>


Construct a :py:class:`dict` to update the game state
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following three keys, at least, must be be returned to the CTGames framework from this function

    :py:attr:`answer_type`
        Use whatever was decided in the previous step, e.g ``AnswerType.Int``, ``AnswerType.MCQ``, and so on.

    :py:attr:`targets`
        This is the list of correct/expected answers.
        It is always in the form of a list, even if there is only one answer.
        Each answer is generally the int or string expected (e.g. 3, 'Apple', etc.), except in the cases of ``AnswerType.MCQ`` and  ``AnswerType.ImgMCQ``, where it is the multiple-choice label corresponding to the correct answer, e.g. 'A', 'B', etc.

    :py:attr:`custom`
        This is the name of the ``GameStateCustom`` object created a couple of steps before.

For MCQ-type games, the following key must also be returned

    :py:attr:`multiple_answers`
        The list of possible answers.

In Game Template Int, this :py:class:`dict` looks like

.. code-block:: python

    updates = {
        'answer_type': AnswerType.Int,
        'targets': [target],  # `targets` is a list of correct answers
        'custom': custom,
        }

In Game Template MCQ, this :py:class:`dict` looks like

.. code-block:: python

    updates = {
        'answer_type': AnswerType.MCQ,
        'multiple_answers': multiple_answers,
        'targets': targets,  # `targets` is a list of correct answers
        'custom': custom,
        }
