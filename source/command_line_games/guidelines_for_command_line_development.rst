.. _sect_guidelines_for_command_line_development:

============================================
Guidelines for command-line game development
============================================

This section gives general guidelines for developing command-line CTGames games.


-------------------------------------
The purpose of the command-line games
-------------------------------------

The command-line version of each game is only run directly by CTGames developers during the game development process. It allows the developer to more efficiently create and test the logic of a game before deploying as a web app. As such, the command-line version of a game does not have to be pretty. However, it should be fully functional, and should expose the internal data structures that would need to be parsed by the web app to create the child-friendly graphics. For example, if the web app is supposed to have a graphical grid, then the command-line game should have a pretty-printed text-based grid.

.. tip:: The web app should have as little code in it as possible. If there is any computation to be done to figure out graphical patterns or animations, that should be done in the logic file, the end result encoded in a text string, and that text string printed in the command-line game. This encoding scheme should be designed by the game developer in such a way that the web app code can parse the text string and simply and directly translate it into graphical instructions (filenames of specific graphic element, for example). The reason for this is that we want the framework to be able to support multiple graphical front ends (e.g. desktop, mobile, and alternative web-based game engines). Since the logic file will be common to all front ends, the less code that will be in the web app, the easier it will be for the game to be deployed with a different front end.

.. tip:: The output text from the command-line game just needs to be the minimum necessary to verify that the game is working. It can make your programming job easier if there is a correspondence between the command-line text and the intended web app graphical output. For example, if the web app calls for the sequence of images, the command-line game can use a sequence of characters, as long as there is a one-to-one correspondence between a character and the web app image it represents. Then the web app could have the minimal code of just going through the sequence of characters one by one and loading a graphic for each.


-----------------------------------------
Simplest command-line games to learn from
-----------------------------------------

One excellent way to learn how to create games with the CTGames framework is to try to understand the code of existing games. Besides the template game, the three simplest games in CTGames are designed to act as learning aids for someone starting out with the framework. They are Just Addition (directory :file:`justaddition`), Just Odd (directory :file:`justodd`), and Just Reverse (directory :file:`justreverse`). Please read and understand the code for these games to get some ideas for how to make a start with your game.


Running command-line versions of other developers' games
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../game_development/include_running_command_line_games.rst
