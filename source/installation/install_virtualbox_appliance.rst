.. _sect_install_virtualbox_appliance:

--------------------------------
Install the VirtualBox appliance
--------------------------------

Unzip the file :file:`CTGamesYYYYMM.zip` (where ``YYYYMM`` are appropriate numerals) that you previously downloaded and for which you performed an integrity check.

.. note:: If you have not downloaded and checked the integrity of :file:`CTGamesYYYYMM.zip` yet, please go to section ":ref:`sect_install_virtualbox_appliance_download`".

Go to the VirtualBox virtual machines (VMs) folder in your home directory (where in each case ``username`` is your username);

    for Windows, this is usually :file:`C:\\Users\\username\\VirtualBox VMs\\`

    for Linux this is usually :file:`/home/username/VirtualBox VMs/`, and

    for macOS this is usually :file:`/Users/username/VirtualBox VMs/`.

If directory :file:`VirtualBox VMs` does not exist in your home directory already, just create it.

Create a new directory inside the :file:`VirtualBox VMs` directory called :file:`CTGamesYYYYMM` (where ``YYYYMM`` is the code used in the filenames, e.g. ``202110``) and move the virtual appliance files there.

Create a new directory anywhere on your computer called :file:`virtualbox_shared` and remember where it is. You can use this directory to share files between your host OS and the Ubuntu guest OS.

If the VirtualBox app is not running, start it, and continue the instructions in the next section of these docs. As usual, if prompted to upgrade VirtualBox, do not upgrade.
