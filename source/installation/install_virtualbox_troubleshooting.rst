.. _sect_install_virtualbox_troubleshooting:

---------------------------------------------
Troubleshooting VirtualBox virtual appliances
---------------------------------------------

Using VirtualBox to develop CTGames games, rather than directly running Ubuntu on your computer, has the disadvantage that virtual hard disks in VirtualBox appliances are less reliable than real hard disk hardware.
This section collects knowledge about how to recover from local Git repository corruption errors, which seem to be the most common, albeit still infrequent, type of error encountered by CTGames developers over the years.
Typically, this type of error causes all :command:`git` commands to fail with an message of the form
::

    error: object file .git/objects/a3/da67e9f4ef6830efeea467239232832cb3ce17 is empty
    fatal: loose object a3da67e9f4ef6830efeea467239232832cb3ce17 (stored in .git/objects/a3/da67e9f4ef6830efeea467239232832cb3ce17) is corrupt

On average, one CTGames developer in ten will encounter one of these errors during five months of development.

.. warning::
    Whenever shutting down a Ubuntu virtual appliance, always select :guilabel:`Power Off...` from within Ubuntu and give the operating system time to gracefully shut down.
    Never close the running virtual machine window, or close the VirtualBox app when a virtual machine in running, or let your laptop/desktop hibernate or shut down while a virtual machine is running.
    This can lead to local Git repository corruption errors.

.. note:: You don't need to read any more of this section if you're newly installing CTGames, but make a note of this web page and come back to it if you ever get a local Git repository corruption errors.


Recovering from :command:`git` error "object file .git/objects/12/345... is empty"
----------------------------------------------------------------------------------

Even with careful use of VirtualBox, local Git repository corruption errors can occur in one's virtual appliance. A range of fixes are suggested with trade-offs between time required and likelihoods of success, concluding with a time-consuming fix that always works.


Quick fix
^^^^^^^^^

The following questions on StackOverflow can be consulted for more details and variations on this quick fix: `How can I fix the Git error "object file ... is empty"? <https://stackoverflow.com/q/11706215>`_ and `Git: "Corrupt loose object" <https://stackoverflow.com/q/4254389>`_.

Run the following to delete all empty files causing the error (it has been suggested that :command:`git fetch -p` should be run instead of :command:`git pull`)
::

    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/
    find .git/objects/ -type f -empty -delete
    git pull
    git fsck --full
    git reflog -n 1
    git status

If the :command:`fsck` command gives errors, or the :command:`reflog` command outputs "fatal: bad object HEAD", or the :command:`status` command gives errors, then this quick fix did not work and go with the slow fix below.
Otherwise, the :command:`status` command should show you any modified files you have waiting to be committed and you can continue working.


Moderate-time fix
^^^^^^^^^^^^^^^^^

.. warning::
    We're going to use a lot of disk space temporarily having two copies of the Git repository.
    If your virtual hard disk is currently more than half full then proceeding with this option is not recommended.
    If you run out of disk space in the virtual hard disk it may resulting in it not being able to boot properly.

The moderate-time fix involves cloning the Git repository again and replacing your corrupted :file:`~/.git/` directory with a newly-cloned copy.
To do this, follow these steps.
Make a copy of your games (where :file:`mygame1`, etc., are the directory names for your games)
::

    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/ctgames/
    cp -r mygame1 ~/Desktop/
    cp -r mygame2 ~/Desktop/
    cp -r mygame3 ~/Desktop/

.. note:: The reason we don't just work with our new Git repository and delete the old one is that the directory tree contains files that are not part of the Git repository (such as database files) that would take quite a lot of time to configure.

Clone the Git repository again (note, this can take a long time!)
::

    cd ~/gitlab.cs.nuim.ie/
    mkdir ctgames-new
    cd ctgames-new/
    git clone git@gitlab.cs.nuim.ie:ctgames/ctgames.git
    cd ctgames
    git status

Delete the corrupted :file:`.git` directory and use the fresh copy of the newly-cloned :file:`.git` directory instead
::

    cd ~/gitlab.cs.nuim.ie/
    rm -rf ctgames/.git
    mv ctgames-new/.git ctgames/

Delete the newly-cloned Git repository using these commands.
When prompted, supply the password you use to sign into the VirtualBox appliance (:command:`sudo` is needed below in the case that you ran the server and files were created by the web server and database process running as root)
::

    cd ~/gitlab.cs.nuim.ie/
    sudo rm -rf ctgames-new
    cd ~/gitlab.cs.nuim.ie/ctgames/
    git pull
    git status

The output from the last command should read something like "On branch master Your branch is up to date with ‘origin/master’. nothing to commit, working tree clean."

If you do not get this message, try the fix specified in the next subsection.
If you do get this message, copy any files from your games in ~/Desktop/ that you had changed but did not :command:`git push` before the problem occurred; you can continue working.


Slow fix
^^^^^^^^

The slow fix involves downloading a brand new virtual appliance, configuring the appliance, configuring Git, and setting up a new SSH key pair.
It takes a long time, but has the highest likelihood to fix the problem.

First, copy of all of the files that you have edited but have not been able to push.
For simplicity, you could copy the current state of all three of your games.
You must make a copy outside your virtual machine (e.g. use the :file:`virtualbox_shared` directory you created earlier, email them to yourself from within the virtual machine, or something else).

Shut down the virtual machine.

In the VirtualBox app, in the left-hand panel, make sure the CTGamesYYYYMM appliance is highlighted and then using the menu click :guilabel:`Machine` ‣ :guilabel:`Settings...`. Then in the text box labelled :guilabel:`Name:` change ``CTGamesYYYYMM`` to ``CTGamesYYYYMM_old``.

Ensuring the renamed "CTGamesYYYYMM_old" appliance is highlighted, click :guilabel:`Machine` ‣ :guilabel:`Remove...` ‣ :guilabel:`Remove only`.

.. note:: The reason we have to remove the current version of the appliance is that VirtualBox will complain if we try add to a virtual disk with the same `UUID <https://en.wikipedia.org/wiki/Universally_unique_identifier>`_ to two different appliances.

It is assumed that you have VirtualBox installed already.
If not, go to section ":ref:`sect_install_virtualbox_virtualbox`" and do all of the steps on that page (but come back here after reaching the bottom of that page; don't click :guilabel:`Next ➩`).

It is assumed that you have previously downloaded the virtual appliance files already.
If not, go to section ":ref:`sect_install_virtualbox_appliance_download`" and do all of these steps on that page.

Go to section ":ref:`sect_install_virtualbox_appliance`" and do all of these steps on that page, and on all following pages, until you have set up your SSH key, tested it, and received the “Welcome to GitLab” message.

Bring the Git repository up to date with
::

    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/bin/
    git pull

Copy any files you changed but didn't push previously, and you can continue working.
