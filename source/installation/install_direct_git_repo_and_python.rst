.. _sect_install_direct_git_repo_and_python:

=========================
Git repository and Python
=========================


-----------------
Related Git tools
-----------------

#. Install gitk and git-gui

    The graphical tools gitk and git-gui are part of a standard full Git installation. In a terminal from any directory, enter
    ::

        sudo apt install gitk git-gui

#. Install file comparison tool

    In a terminal from any directory, enter
    ::

        sudo apt install meld

..
        **HIDDEN**
        In a terminal from any directory, enter
        ::

            sudo apt install nautilus-compare

        If this gives errors (e.g. "E: Unable to locate package nautilus-compare" on Ubuntu 20.04) then try instead
        ::

            sudo add-apt-repository ppa:boamaod/nautilus-compare
            sudo apt-get update
            sudo apt install nautilus-compare

        Now, restart Nautilus (the default file manager with Ubuntu) with
        ::

            nautilus -q

        and you should see the two context menu items :guilabel:`Compare Later` and :guilabel:`Compare to...` when you right-click on a file in Nautilus.
        These context menu items allow one to conveniently pick out two text files for a graphical comparison of their text differences.
        **END OF HIDDEN**


.. tip:: To compare two files with Meld, select the two of them in the file manager, right click, and then select to open them with Meld.


--------------------------
Cloning the Git repository
--------------------------

#. Clone the existing git repository

    These instructions will clone the git repository into a well-known location in your home directory. If you choose a different directory, appropriately change the directory given in the rest of documentation. In a terminal from any directory, enter
    ::

        cd ~
        mkdir gitlab.cs.nuim.ie
        cd gitlab.cs.nuim.ie
        mkdir ctgames
        cd ctgames
        git clone git@gitlab.cs.nuim.ie:ctgames/ctgames.git
        cd ctgames
        git status

    The output from the last command should read something like "On branch master Your branch is up to date with 'origin/master'. nothing to commit, working tree clean."

#. View recent commits to the git repository

    This gives you an idea of the activity in the git repository. All team members code changes are visible. In the terminal, from any directory in the cloned git repository, enter
    ::

        gitk --all


----------------------------------------------------------------------
Install Miniconda (a mini version of the Anaconda Python distribution)
----------------------------------------------------------------------

..
        **HIDDEN**
        1. Download Miniconda

        Point Firefox to

            `<https://docs.conda.io/en/latest/miniconda.html#linux-installers>`_

        and download :guilabel:`Python 3.9 Miniconda3 Linux 64-bit` into your :file:`~/Downloads/` directory.
        Notice the SHA256 hash of this download is provided next to the download link.

        Wait for the file to download fully. Press the key combination :kbd:`Ctrl-Alt-t` to open a *new* terminal, and change to the :file:`Downloads` directory by entering
        ::

            cd Downloads
            ls

        In the terminal, calculate the SHA256 hash of the downloaded file by typing the following (without pressing :kbd:`Enter`)
        ::

            sha256sum Min

        Press the :kbd:`Tab` key.
        If you downloaded the file correctly, the command will autocomplete to something like :command:`sha256sum Miniconda3-py39_4.10.3-Linux-x86_64.sh`.
        Press :kbd:`Enter` to calculate the SHA256 hash.
        If this SHA256 hash matches the hash on the web page (checking the first five and last five digits is enough), then the download completed successfully, otherwise please download again.
        **END OF HIDDEN**


1. Download Miniconda

    Press the key combination :kbd:`Ctrl-Alt-t` to open a *new* terminal and enter (two of these lines are very long -- make sure to scroll to the right to copy each line in full)
    ::

        mkdir -p ~/miniconda3
        cd ~/miniconda3
        wget https://repo.anaconda.com/miniconda/Miniconda3-py311_23.5.2-0-Linux-x86_64.sh -O miniconda.sh
        echo "634d76df5e489c44ade4085552b97bebc786d49245ed1a830022b0b406de5817  miniconda.sh" | shasum -a 256 --check

    You should get the response "miniconda.sh: OK". If not, the file was not downloaded correctly.

2. Install Miniconda

    In the same terminal, enter (make sure to notice the dot at the end of the first line)
    ::

        bash miniconda.sh -b -u -p .
        rm -rf miniconda.sh

    ..
            **HIDDEN**
            Press the :kbd:`Tab` key. If you downloaded the file correctly, the command will autocomplete to something like :command:`bash Miniconda3-py39_4.10.3-Linux-x86_64.sh`. Press :kbd:`Enter`.

            Press :kbd:`Enter` when prompted to view the licence terms.
            The :kbd:`Space` key can be used to scroll through the licence terms.
            Accept the licence terms, when asked, by typing :command:`yes`.

            When asked about a location, press :kbd:`Enter` to accept the default location.
            Wait for it to install.

            If asked do you want to initialise Miniconda, type :command:`yes`.
            **END OF HIDDEN**

    Initialise Miniconda for the bash shell by entering, in the same terminal, the command
    ::

        bin/conda init bash


3. Test Miniconda

    After installation, close the terminal (by entering :command:`exit`), reopen a new terminal (e.g. :kbd:`Ctrl-Alt-t`), and test your installation by entering
    ::

        conda list

    You should see a list of packages installed, including two with names "conda" and "python". Ensure Anaconda Python has been installed by default, by entering
    ::

        python

    If the first line is of the form "Python 3.11.4 (main, Jul  5 2023, 13:45:01) [GCC 11.2.0] on linux" then Miniconda was installed correctly. Press :kbd:`Ctrl-d` to exit Python.

    ..
            **HIDDEN**
            Keep your terminal open.
