.. _sect_install_options:

===============================================
Explanation of installation options for CTGames
===============================================

In order to develop games for CTGames, you need to be a Maynooth University Department of Computer Science student with an account on the local Gitlab instance `<https://gitlab.cs.nuim.ie/>`_, and you need to be added as a developer on the project `CTGames project <https://gitlab.cs.nuim.ie/ctgames/ctgames>`_.

The three installation options for the developer have different trades-off between functionality, performance, and installation effort.


.. _sect_install_options_zero:

----------------------
Option 1: Zero-install
----------------------

The zero-install option uses a web browser for all development steps. This option is explained in section ":ref:`sect_install_zero`".

Advantages:

    - you can be up and running very quickly
    - works from any device (even Android/iOS mobile devices with only 2 GB of memory)

Disadvantages:

    - you cannot use this approach to try the command-line versions of games, or develop your own
    - any changes made to the Git repository can take 10 minutes before they are visible in the deployed version, so it's not practical as a development environment, but can be useful for minor edits to an existing game
    - if you don't remember to stage your commits using the appropriate provided GitLab web-based tools, this approach can make the Git history look quite noisy with many inconsequential tiny commits (that are not convenient to squash into a single commit since they have already been pushed to the remote repository)


------------------------------------
Option 2: VirtualBox virtual machine
------------------------------------

This option involves using VirtualBox to run a virtual machine that has the full development environment already installed and the Git repository already cloned. The option is explained in section ":ref:`sect_install_virtualbox`".

Advantages:

    - you can be up and running reasonably quickly (15 minutes to 1 hour to download the virtual appliance, depending on your internet connection, plus 30 minutes to 1 hour following the instructions)
    - works on Windows, macOS, and GNU Linux (but not mobile devices)
    - no need to take any risks changing how your computer boots up or buying a separate hard disk drive

Disadvantages:

    - requires up to 30 GB of disk space and a computer with at least 4 GB of memory (8 GB memory recommended because of the virtual machine overhead)
    - the performance of your :abbr:`IDE (integrated development environment)` and web browser might be sluggish if your your PC/laptop is old
    - although extremely reliable, VirtualBox is less reliable than real hardware, so your local Git repository is more likely to become corrupted on a virtual hard disk than on a real hard disk (frequent pushes to the remote Git repository is the best insurance against virtual disk errors or the virtual appliance becoming inaccessible for any reason)


-----------------------------------
Option 3: Directly on your computer
-----------------------------------

This option involves installing all parts of the development environment (Python, Git, Docker, and so on) and cloning the Git repository directly onto your computer. The option is explained in section ":ref:`sect_install_direct`".

Advantages:

    - if you have GNU Linux installed already, this is the best option
    - you get maximum performance from your hardware and memory

Disadvantages:

    - you have to install Ubuntu onto your PC/laptop, which comes with the risk that you might accidentally delete your existing operating system if you don't pay attention (I would recommend buying a separate hard disk drive for Ubuntu if possible)
    - requires a little over 20 GB of disk space (30 GB recommended if you wish to install other apps) and a computer with 4 GB of memory (less than needed for the virtual machine option)
    - takes about 45 minutes to 90 minutes to install everything, depending on your internet connection and familiarity with the command line

--------------------------------------------
Option 2a: Custom VirtualBox virtual machine
--------------------------------------------

There is of course a do-it-yourself version of "Option 2" which involves setting up your own VirtualBox appliance with Linux and install the CTGames development environment yourself using the "Option 3" steps. This option might be taken by someone who wants to develop in a virtual environment but does wish to use the installation of Ubuntu in the VirtualBox appliance supplied by their project supervisor. However, a more likely use of these instructions is for the project maintainer to set up the VirtualBox appliance used for "Option 2". As such, the steps are given in short form as it is assumed only someone familiar with CTGames would undertake such an installation.

The steps, in short form, are as follows.

VirtualBox
----------

- Install the VirtualBox program as described in Option 2
- Click "New"
- Name: CTGames202310
- Type: Linux
- Version: Ubuntu 64 bit
- Memory: 3072 MB
- Hard disk: Create a virtual hard disk now
- Hard disk file type: VDI
- Storage: Dynamically allocated
- File location: leave as suggested / File size: 30 GB
- Click :guilabel:`Create`
- Settings: General: Advanced: Shared clipboard: Bidirectional
- Settings: System: Motherboard: Base Memory: set to maximum of green values
- Settings: System: Processor: Processor(s): set to maximum of green values
- Settings: System: Processor: Execution Cap: 100%
- Settings: Display: Screen: Video Memory: set to maximum allowed
- Settings: Shared Folders: add your Desktop as a shared folder, select Read-only, select Auto-mount
- Start the machine and select the file "ubuntu-22.04.3-desktop-amd64.iso" downloaded from http://releases.ubuntu.com/22.04/ with SHA256 from http://releases.ubuntu.com/22.04/SHA256SUMS. Run :command:`echo "a435f6f393dda581172490eda9f683c32e495158a780b5a1de422ee77d98e909  ubuntu-22.04.3-desktop-amd64.iso" | shasum -a 256 --check`

Ubuntu
------

- Choose "Install Ubuntu"
- Choose "English (UK)"
- Select "Normal installation" and "Download updates..." and "Install third-party software..."
- Choose "Erase disk and install Ubuntu" and click "Continue"
- Choose "Dublin"
- Name: Dev
- Choose a password that will be circulated to students and select "Require my password..."
- Restart when prompted
- Software & Updates: Ubuntu Software: Restricted Software (multiverse)
- Software & Updates: Ubuntu Software: Download from: Main server
- # Software & Updates: Other Software: Canonical Partners
- Software & Updates: Updates: All updates, Every two weeks, Display immediately, Display every two weeks, Never
- Software Updater: Update all / Update now
- :command:`sudo apt install gcc make perl`
- Devices: Insert Guest Additions CD Image...
- If it does not run automatically, from a console run :command:`./autorun.sh`
- Power off: restart

Desktop
-------

- Eject guest additions CD
- Ubuntu Software: Install Chromium
- Ubuntu Software: Install Inkscape
- Ubuntu Software: Install vlc
- As "Favourites", leave only file manager, Firefox, Chromium, Text editor, Terminal, Inkscape, and Trash in the Dock
- Firefox: Bookmarks Toolbar: Always Show
- Firefox: Bookmarks Toolbar: add CTGames (local) [http://127.0.0.1:8080/]
- Firefox: Bookmarks Toolbar: add CTGames docs [https://ctgames.readthedocs.io/]
- Firefox: Bookmarks Toolbar: add GitLab [https://gitlab.cs.nuim.ie/]
- Firefox: Settings: Open previous windows and tabs
- Firefox: Settings: Make default browser
- Chromium: On start-up: Continue where you left off
- Chromium: Bookmarks: add CTGames (local) [http://127.0.0.1:8080/]
- Chromium: Bookmarks: add CTGames docs [https://ctgames.readthedocs.io/]
- Chromium: Bookmarks: add GitLab [https://gitlab.cs.nuim.ie/]
- Settings: Appearance: Show Personal Folder: Off
- Settings: Region: Language: English (UK)
- Settings: Power: Blank Screen: Never
- Settings: Power: Automatic Suspend: Off
- Settings: Power: Show Battery Percentage
- :command:`ln -s ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/bin/ ~/Desktop/CTGames\ bin`
- :command:`sudo apt update`
- :command:`sudo apt upgrade`
- :command:`sudo apt install ubuntu-restricted-extras`
- :command:`sudo apt install gnome-tweaks`
- Tweaks: Top Bar: Week Day & Week Numbers
- Tweaks: Windows: Attach Modal Dialogs: Off
- Open Nautilus and press :kbd:`Ctrl-h`
- :command:`cd ~/Templates` and :command:`touch "New text file.txt"`

GitLab
------

- :command:`ssh-keygen -t ed25519 -C "CTGames VirtualBox YYYYMM"`
- Enter filename :file:`/home/dev/.ssh/gitlab_cs_deploy_key`
- No password
- :command:`cat ~/.ssh/gitlab_cs_deploy_key.pub`
- Go to https://gitlab.cs.nuim.ie/ctgames/ctgames/-/settings/repository
- Create new Deploy Key: Title: CTGames Deploy VirtualBox YYYYMM
- Paste the contents of the .pub file
- Choose: no write permissions (pull only)
- Click :guilabel:`Add key`
- :command:`ssh -T git@gitlab.cs.nuim.ie`
- "Are you sure you want to continue connecting?" :command:`yes`

.. note:: For this job we could also use a more light-weight token (equivalent to a password) rather than a SSH key, and use git to store the token after first use (https://stackoverflow.com/a/35942890).
    GitLab seems to offer two suitable options here (https://docs.gitlab.com/ee/security/token_overview.html), Deploy Tokens and Project Access Tokens, that have some overlap in functionality.
    A Deploy Token can have read-only access to clone a repository, but it's not clear from the docs if it can subsequently also pull.
    A Project Access Token can read from, and also optionally write to, the repository, but this creates a new bot member of the project.
    In both cases, some extra work needs to be done if it is not desirable that git stores the token in plain text.


Git repository, Python, and teacher portal
------------------------------------------

- :command:`sudo apt install git`
- Follow the steps in section ":ref:`sect_install_direct_git_repo_and_python`"
- Follow the steps in section ":ref:`sect_install_direct_portal`"
- Put the teacher name/email/password and class member PINs in :file:`~/Desktop/Player PINs.txt`"

Software development environment
--------------------------------

- Follow the steps in section ":ref:`sect_install_direct_development_env`"
- :command:`cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/`
- :command:`python setup.py`

Deployment
----------

- Shut down the virtual machine.
- Zip the virtual disk image file :file:`CTGamesYYYYMM.vdi` as :file:`CTGamesYYYYMM.zip`
- :command:`md5sum CTGamesYYYYMM.zip >> CTGamesYYYYMM_zip_digests.txt`
- :command:`sha256sum CTGamesYYYYMM.zip >> CTGamesYYYYMM_zip_digests.txt`
- Send :file:`CTGamesYYYYMM.zip`, :file:`CTGamesYYYYMM.vbox`, and :file:`CTGamesYYYYMM_zip_digests.txt` to students
