.. _sect_install_virtualbox_gitlab_deploy_key:

--------------------------------------------------
Before configuring GitLab: removing the Deploy Key
--------------------------------------------------

A GitLab Deploy Key has been used to clone the CTGames GitLab repository into the the Ubuntu virtual appliance.
This Deploy Key as read-only access to the GitLab repository and so can't be used for development.
Before installing your own SSH key (as described later in section ":ref:`sect_install_virtualbox_gitlab`") it is a good idea to remove the Deploy Key.
Otherwise, at some future time the Ubuntu SSH key agent may use the Deploy Key when authenticating your access to the GitLab repository and your :command:`git push` command will fail.
Check the filename of the Deploy Key pair with
::

    cd ~/.ssh
    ls -al

and then remove both the private and public parts of the key pair with (use whatever key pair filename you find with the previous command)
::

    rm gitlab_cs_deploy_key
    rm gitlab_cs_deploy_key.pub

Shut down Ubuntu (see warning below) and restart; our aim here is to make sure the Ubuntu SSH key agent recognises that the Deploy Key has been deleted -- if you are a Ubuntu expert you may know of a light touch and reliable alternative (e.g. :command:`pkill gnome-keyring`).

.. warning::
    Whenever shutting down a Ubuntu virtual appliance, always select :guilabel:`Power Off...` from within Ubuntu and give the operating system time to gracefully shut down.
    Never close the running virtual machine window or close the VirtualBox app when a virtual machine in running, or let your laptop/desktop hibernate or shut down while a virtual machine is running.
    This can lead to local Git repository corruption errors (section ":ref:`section sect_install_virtualbox_troubleshooting`" covers what to do if this happens).
