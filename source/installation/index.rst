============
Installation
============

This chapter describes the three options available to develop games for CTGames, and then goes into detail on each.


.. toctree::
    :maxdepth: 1
    :name: toc-installation

    install_options
    install_zero
    install_virtualbox
    install_direct

.. history
.. authors
.. license
