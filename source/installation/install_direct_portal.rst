.. _sect_install_direct_portal:

=====================================
Setting up the CTGames teacher portal
=====================================

At this point you should have already installed the Miniconda (Anaconda) Python distribution and cloned the CTGames git repository.

Setting up the web-based CTGames teacher portal allows one to run the web app versions of games locally one one's computer.


----------------
Install software
----------------


Create the Conda environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Conda** allows us to keep our CTGames installation separate from any other uses of Python on your computer.

.. tip:: A `cheat sheet <https://docs.conda.io/projects/conda/en/latest/user-guide/cheatsheet.html>`_ can be useful to learn more about **Conda** environments.

Open a terminal and create a **Conda** environment called "ctgames" by entering
::

    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/
    conda env create --file conda_environment.yml

..
        **HIDDEN**
        Aside: How I created this file:
        conda activate ctgames
        conda env export | grep -v "^prefix: " > conda_environment.yml

        Aside: To refresh the installed packages (possibly after updating the conda_environment.yml file):
        conda activate ctgames
        conda env update --file conda_environment.yml --prune --verbose
        **END OF HIDDEN**


Get a list of your **Conda** environments with
::

    conda env list

Activate the ctgames environment with
::

    conda activate ctgames

.. note::
    In September 2020, Anaconda removed the 2020 version of the package **Black** from Conda and replaced it with the 2019 version.
    That is why in the .yml file we specify **pip** rather than **conda** to install the 2020 version of **Black**.
    If you encounter package version problems, and notice (by running :command:`conda list` within the **Conda** environment) that a package version has been installed that is older than that specified in the .yml file, one fix might be
    ::

        conda remove package_name
        pip install package_name


Install docker-related packages
-------------------------------

**Docker** needs to be set up.
**Docker** allows us to build, ship, and run distributed applications.
First, update existing packages and remove any existing version of docker
::

    sudo apt update
    sudo apt upgrade
    sudo apt remove docker docker-engine docker.io docker-ce
    sudo apt upgrade
    sudo apt autoremove

Install **Docker**, by adding its official GPG key, ensuring we have the correct key by checking its well-known fingerprint (obtained from `<https://docs.docker.com/engine/install/ubuntu/>`_), and registering the stable **Docker** repository with Ubuntu
::

    sudo apt install apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt update
    sudo apt install docker-ce
    sudo docker -v

The version information should not be older than "Docker version 24.0.6, build ed223bc".
If everything has installed correctly, you should see a message that includes "Hello from Docker!
This message shows that your installation appears to be working correctly" after running the following
::

    sudo docker run hello-world



------------------------------------
Building and starting the containers
------------------------------------

After all the required items have been installed and set up, the next steps get the docker instances (webserver, web framework, database) up and running.


.. _sect_install_direct_portal_flask_secrets:

Create the flask secret key
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The secret key is used to sign cookies during player sessions.
This secret key will not be pushed to the git repository and so is specific to each installation.
To generate it, in a terminal from any directory, enter
::

    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/bin/server/docker/app/DB-Model/
    conda activate ctgames
    python create_flask_secret_key.py

You should get the response "File flask_secrets.py successfully created.".


Build the containers
^^^^^^^^^^^^^^^^^^^^

This should print some timings and network speeds, but otherwise not give any errors
::

    curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` > ./docker-compose

**Docker Compose** allows you to define a set of containers and their interdependencies in the form of a YAML file
::

    sudo mv ./docker-compose /usr/bin/docker-compose
    sudo chmod +x /usr/bin/docker-compose
    sudo docker-compose build --no-cache


Start and stop the docker server
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Build, (re)create, start, and attach the appropriate **Docker** containers.

.. note::
    You might get an error message "``/etc/init.d/postgresql: command not found``" when you run the lines below.
    This is fine; the first line is included in case you happen to have **PostgreSQL** installed locally (outside the **Docker** container) for some other purpose.
    Also, errors might be returned since the database does not have any data yet; if so, just ignore them.

::

    sudo /etc/init.d/postgresql stop
    sudo docker-compose up

Wait for a few seconds until the terminal output seems to have finished, or until the last line in the terminal reads something like "``flaskapp_1  |  * Debugger PIN: 157-167-011``" (the 9 digit code can differ).
Then, shut down the server in the **Docker** container with :kbd:`Ctrl-c`.

The **Docker** server takes a second or two to shutdown. When it does, start the server again in order to be able to create the database using
::

    sudo docker-compose up


Create the database tables
^^^^^^^^^^^^^^^^^^^^^^^^^^

We will create the appropriate tables in the **PostgreSQL** database "MU_CT_Task_Web_DB".
We need the **Docker** container to be up in order to do this, so keep the container running from the previous step and open a *new* terminal in any directory (:kbd:`Ctrl-Alt-t`), and enter
::

    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/bin/server/docker/app/DB-Model/
    conda activate ctgames
    python createDBTable.py

..
        **HIDDEN**
        Previously, we installed psycopg2, a PostgreSQL adapter for Python, using
        conda install -c anaconda psycopg2
        [Note to maintainers of this file. The location of this package moves frequently.
        In early 2020 this used to be :command:`conda install -c conda-forge psycopg2-binary` and pre-2020 this used to be :command:`conda install -c conda-forge psycopg2`
        ]


Open up the link to CTGames portal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Point your Firefox browser to the URL below to display the portal to verify the webserver is running

    `<http://127.0.0.1:8080/teachers>`_

If you see a teacher sign in page then the webserver is running.


---------------------------------
Create School and Teacher account
---------------------------------

A school account needs to be set up before a teacher's account can be created.


Open the school sign-up page on the portal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::
    If you get any error messages during the following steps, please copy/screenshot them and forward to your supervisor.
    There are limits on the number of schools/teachers that can be registered and these limits may need to be raised periodically.

Fill out the school sign-up form to register a school

    - Visit `<http://127.0.0.1:8080/schools_signup>`_
    - Enter a school name and school email
    - Enter a school roll number that is seven numerals long
    - Create a password and click :guilabel:`Sign up`
    - When the school account is created successfully, make note of the "School code"


Open the teacher sign-up page on the portal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. tip::
    When the school is signed-up (previous step) it will be assigned a school code.
    If you forget this code, one way to access it is to open the database using **pgAdmin3** and examine the "school" table.

To sign up as a teacher

    - Visit `<http://127.0.0.1:8080/teachers_signup>`_
    - Enter a teacher username and teacher email
    - Enter a school roll number that is seven numerals long
    - Enter the relevant school code that was given during school sign-up
    - Enter a password and click :guilabel:`Sign up`

To create a class

    .. note:: As of October 2021, the steps below cause an error
        ``AttributeError("function/symbol 'pango_context_set_round_glyph_positions' not found in``
        ``library 'libpango-1.0.so.0': /usr/lib/x86_64-linux-gnu/libpango-1.0.so.0: undefined``
        ``symbol: pango_context_set_round_glyph_positions")``.
        However, the class of students is created successfully.
        Please make a note of the class members' IDs and PINs printed to the terminal at class creation time.

    - Visit `<http://127.0.0.1:8080/teachers>`_ and sign-in with your teacher username and password from above
    - Click :guilabel:`Home` if not automatically sent to `<http://127.0.0.1:8080/teachers_home>`_
    - If no classes have been created yet, click :guilabel:`Create new class` and choose a name, description, and a size for the class ("5" class members should be fine)

To find out class members' PINs

    .. note:: As of October 2021, this functionality is temporarily not working. Class members' PINs are printed to the terminal at the time that each class is created.

    - When signed in as a teacher, click :guilabel:`Class details`
    - Find the relevant class in the list and click :guilabel:`Print list`
    - A PDF listing of the IDs and PINs for each class members will open in a new tab
    - Make a note of at least one class member's ID and PIN

Find the terminal that contains the running server in the **Docker** container (that you started a little while ago when you typed in the command :command:`sudo docker-compose up`) and shut it down using :kbd:`Ctrl-c`.

Now that you have a teacher's sign-in details (username and password) and a class member's account details (ID and PIN) you can test that the virtual appliance has been configured correctly by starting the CTGames server and playing some of the web app games developed by previous students (explained next).


Running locally the web app versions of other developers' games
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../web_apps/include_running_web_app_games.rst
