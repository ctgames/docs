.. _sect_install_virtualbox_configure:

--------------------------------------------------------
Configuring the virtual appliance for your host computer
--------------------------------------------------------

In VirtualBox, click :guilabel:`Machine` ‣ :guilabel:`Add...` and choose the :file:`.vbox` file that you saved earlier.

Choose the CTGames virtual machine from the list of available machines and then click on the yellow cog labelled :guilabel:`Settings`. A "CTGamesYYYYMM - Settings" window should pop up.

In the :guilabel:`System` ‣ :guilabel:`Motherboard` section give this virtual machine the maximum recommended amount of :guilabel:`Base Memory` as indicated by the green bar.

In the :guilabel:`System` ‣ :guilabel:`Processor` section give this virtual machine the maximum recommended number of CPUs as indicated by the green bar.

In the :guilabel:`Display` ‣ :guilabel:`Screen` section give this virtual machine the maximum recommended amount of :guilabel:`Video Memory` as indicated by the green bar.

In the :guilabel:`Shared Folders` section, select the :file:`/home/tomn/Desktop` shared directory that is there already and click the little :guilabel:`x` button to remove it.

Still in the :guilabel:`Shared Folders` section, click the little :guilabel:`+` button to add a new shared directory.
As the :guilabel:`Folder Path` choose the :file:`virtualbox_shared` directory that you created previously.
Check :guilabel:`Auto-mount` and click :guilabel:`OK`.

Click :guilabel:`OK` to close the "CTGamesYYYYMM - Settings" pop-up window.

Choose the CTGames virtual machine from the list of available machines and then click on the green arrow labelled :guilabel:`Start` to start the virtual machine.

.. admonition:: Error handling

    If you start the virtual machine on a computer that has never hosted a virtual machine before, you may encounter an error of the form "Failed to open a session for the virtual machine... Not in a hypervisor partition... AMD-V is disabled in the BIOS (or by the host OS)."
    There are several causes and solutions for this, related to the BIOS and operating system settings for your AMD chip.
    It would be best to enter these terms (without the quotes) into your favourite search engine and follow the current most popular advice: "virtualbox amd virtualisation disabled in bios".
    The equivalent error message for computers with Intel chips is of the form "VT-x is disabled in the BIOS".


When you start the CTGames virtual machine for the first time you may see two messages regarding "Auto capture keyboard" and "mouse pointer integration".
Just click the :guilabel:`\\` button on each that has the tooltip "Do not show this message again."

When Ubuntu starts, sign in with the password given to you by your supervisor.
Open a terminal (also called a shell) by holding down the three keys :kbd:`Ctrl-Alt-t`, or clicking the shortcut on the Launcher with the tooltip "Terminal".
Resize this terminal window as large as you need.

.. tip:: If you want to hibernate the virtual machine at any point with the
    intention to continue from where you left off (for example, if you're in
    the middle of a CTGames web app game) close the main VirtualBox app (the
    window with the title "CTGamesYYYYMM (Running) - Oracle VM VirtualBox")
    and choose the option with the label :guilabel:`Save the machine state`.

.. tip:: Your Ubuntu guest OS is configured to automatically check for
    updates each week. If you get a pop-up window asking you to install
    updates, you should accept. Sometimes you will be asked for the sign-in
    password. Sometimes you may be asked to shutdown and restart the OS. If you
    encounter an error with CTGames, by keeping your Ubuntu guest OS
    up-to-date, it will be easier for another person to replicate the error
    exactly in order to more quickly find a fix.

.. tip:: If anyone else could have access to your copy of the virtual
    appliance, you should change the Ubuntu user sign-in password, because you
    will be storing a private SSH key in the appliance. One reason you might
    reasonably choose not change the password is if your virtual appliance is
    on a computer for which only you have the password and you use disk-level
    encryption.

..
    **HIDDEN**
    Previously I had argued that protecting the data was not necessary, with:
    Since it is assumed that
    - you will not store or record any non-CTGames personal information in the virtual appliance (internet shopping history, for example),
    - you will push everything you develop to the remote git repository at least at the end of each development period, and
    - the code for your game will be open sourced anyway,
    in practice the strongest reason to change your password might be that it is just good practice to do so.

.. note::
    The virtual appliance is shipped with one user, with a username of ``dev``.
    There is no need to create a new user for yourself or change the username.
    In fact, some of the steps that follow are more convenient if you do not change the username.
