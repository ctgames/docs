.. _sect_install_virtualbox_virtualbox:

---------------------
Installing VirtualBox
---------------------

Point a browser to `<https://www.virtualbox.org/wiki/Download_Old_Builds_6_1>`_

Download VirtualBox 6.1.38 (released September 02 2022) for the operating system on your computer.
It may work on later versions of VirtualBox, but this has not been tested.
To speed you up, here are direct links for the following operating systems:

    - Windows `<https://download.virtualbox.org/virtualbox/6.1.38/VirtualBox-6.1.38-153438-Win.exe>`_

    - Mac OS / Intel `<https://download.virtualbox.org/virtualbox/6.1.38/VirtualBox-6.1.38-153438-OSX.dmg>`_

    - Ubuntu 22.04 `<https://download.virtualbox.org/virtualbox/6.1.38/virtualbox-6.1_6.1.38-153438~Ubuntu~jammy_amd64.deb>`_

Install VirtualBox. Accept each of the default options during install.

Start the VirtualBox app.
If prompted to upgrade VirtualBox, do not upgrade.
If we all use the same version of VirtualBox it is one less variable to deal with if trying to figure out why something does not work.

..
        **HIDDEN**
        When VirtualBox has started, click :guilabel:`File` ‣ :guilabel:`Preferences...` ‣ :guilabel:`Update` and uncheck the box :guilabel:`Check for updates`.
        Click :guilabel:`OK` to close the "VirtualBox - Preferences" pop-up window.

In the Launcher (the tool bar running the full length of the left-hand side of the screen) find the VirtualBox icon, right-click it, and choose :guilabel:`Add to Favourites`.

You can choose to close the VirtualBox app or keep it running (you'll need it soon again).
