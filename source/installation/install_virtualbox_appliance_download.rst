.. _sect_install_virtualbox_appliance_download:

----------------------------------------------
Download virtual appliance and check integrity
----------------------------------------------

Your supervisor will give you instructions for how to download a `virtual appliance <https://en.wikipedia.org/wiki/Virtual_appliance>`_.
This is a 30 GB pre-configured virtual machine image that VirtualBox can run as a virtual computer.
The virtual appliance has Ubuntu 22.04.3 LTS, a minimal installation of the Anaconda Python distribution (Miniconda) with Python 3.9.5, docker (with containers for a Flask WSGI web server, an nginx web server, and a PostgreSQL database server), the PyCharm Python :abbr:`IDE (integrated development environment)` pre-configured for the coding style adopted by CTGames, and a cloned copy of the CTGames remote Git repository with a pre-installed deployment key that allows the latest changes to be pulled.

.. note::
    The pre-installed deployment key will allow you to pull the latest changes from the Git repository but will not allow you to push your modifications.
    For that, you'll have to configure your own SSH key, as described later in the docs.
    (If you are installing the CTGames virtual appliance you should follow the steps below, but if you are looking to skip to the information related to VirtualBox-related SSH keys, it can be found in section ":ref:`sect_install_virtualbox_gitlab_deploy_key`".)

The virtual appliance is shipped as three separate files:

    - An XML settings file describing the hardware for the virtual machine (:file:`CTGamesYYYYMM.vbox`)

    - The zipped contents of the virtual hard disk (:file:`CTGamesYYYYMM.zip`)

    - A digest file (:file:`CTGamesYYYYMM_zip_digests.txt`)

All three filenames should have the same ``YYYYMM`` code, e.g. ``202310``.
Check that the zipped virtual hard disk has been downloaded correctly with one of the digests provided in the :file:`CTGamesYYYYMM_zip_digests.txt` file.
It might take a couple of minutes to check the digest, so be patient.
Then unzip this zip file.

.. tip:: In Windows, the unzipped file may appear inside a newly created directory.
    If so, you must move it to the same directory as the :file:`.vbox` file.

.. tip:: In Linux, for an automated digest check run
    ::

        md5sum --check CTGamesYYYYMM_zip_digests.txt 2>&1 | grep OK
        sha256sum --check CTGamesYYYYMM_zip_digests.txt 2>&1 | grep OK

    Each should return ``CTGamesYYYYMM.zip: OK`` if the file is not corrupted.

    In Windows, run
    ::

        CertUtil -hashfile CTGamesYYYYMM.zip MD5
        CertUtil -hashfile CTGamesYYYYMM.zip SHA256

    and manually compare the two digests that are returned with those in the digest file :file:`CTGamesYYYYMM_zip_digests.txt`.

    In both Linux and Windows, you should run these commands from the directory containing the file :file:`CTGamesYYYYMM.zip`, and replace ``YYYYMM`` in each command with the appropriate numerals.

.. tip:: Don't delete the zip file if you can spare the space. If your virtual hard disk or your git repository ever gets corrupted you can save time by unzipping a new copy rather than having to download and check the integrity again.

.. tip:: Don't delete the zip file if you can spare the space. If your virtual hard disk or your git repository ever gets corrupted you can save time by unzipping a new copy rather than having to download and check the integrity again.
