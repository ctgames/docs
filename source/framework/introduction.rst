.. _chap_ctgames_framework:

.. centered:: Chapter: CTGames framework

=======================================
Under the hood of the CTGames framework
=======================================

It is possible to successfully develop games within the CTGames framework without knowing many details about the framework, however for completeness, this section is included in the docs.
Below is a summary of how game code and framework code interact.
In section ":ref:`framework_separation`" that follows, a high-level overview of the components in the CTGames framework is given.


-----------------------------------------------
Interaction between the framework and your game
-----------------------------------------------

Rather than the CTGames framework being an `API <https://en.wikipedia.org/wiki/API>`_ that is called by the functions you write for your game, each game exposes an API that is called by the CTGames framework.
The API is strictly defined, in that each module and top-level function you write must follow particular naming conventions, with strictly defined parameters and returned types.
Then, when the framework is run, the framework loads your game's modules and runs your game's functions at the appropriate times (creating a new round, checking the player's input, and so on).

Specifically, for your command-line game in directory :file:`x`, when you run :command:`python cli.py x` from within the :file:`CTGames/bin` directory, the following occurs

    - the framework imports your game's logic, and
    - the framework call's your game's functions at the appropriate times.

Similarly, for web app games, on the server side the framework is run first, which starts the web server.
When a player clicks a game icon in the game menu, the following occurs

    - based on the icon clicked, the framework imports the appropriate web app and logic modules, and
    - the framework calls your game's functions at the appropriate times.
