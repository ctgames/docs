.. _sect_namedtuple_inputdomelements:

===========================================
The namedtuple :py:class:`InputDOMElements`
===========================================

(defined in :file:`ctgamestypesdom.py`)

This section explains the namedtuple :py:class:`InputDOMElements` that is returned from function :py:func:`format_player_answer_area`.

The namedtuple is used to store lists of values and display properties (graphical element types, dimensions, orientations, CSS styling properties) to completely specify the area on the web page that the player interacts with to specify their answer.

The game developer builds up appropriate sequences of values, formats them as an instance of :py:class:`InputDOMElements`, returns it from :py:func:`format_player_answer_area`, and the CTGames framework displays it in the appropriate position in the web app (referred to as the player answer area).


------------------------
Fields in the namedtuple
------------------------

The default value of each field is ``None``. Only those fields that you want to use need be assigned values in your code.

:py:attr:`dom_elem_type`
    :py:class:`InputDOMElementType` attribute: The type of input DOM elements, e.g. ``InputDOMElementType.RADIOBUTTONS``.
    See Section :ref:`sect_webapp_class_inputdomelementtype` for more details.

:py:attr:`dom_elem_options`
    seq of :py:class:`InputDOMElementOptions` attributes: a sequence of options that influence how the DOM elements behave or are displayed, e.g. laid out vertically with ``InputDOMElementOptions.VerticalList``.
    See Section :ref:`sect_webapp_class_inputdomelementoptions` for more details.

:py:attr:`labels`
    seq of str: a sequence of strings
    specifying the label to add to the DOM before each element (e.g. a
    ``SELECT`` box). The length of :py:attr:`labels` will be used to determine how
    many identical copies of the element will be added to the DOM. If
    :py:attr:`labels` is ``None`` then it is assumed that one copy is
    required.

:py:attr:`preamble`
    seq of str: the introductory text
    one would like before the sequence of elements. Each element of the
    sequence is considered a separate paragraph. It is displayed such
    that paragraph breaks appear only between different elements of the
    sequence. The programmer should add a blank string to the beginning
    (respectively, end) of the sequence if they wish to have an extra
    paragraph break before (respectively, after) the preamble text.

:py:attr:`options`
    iterable of pairs: an iterable of pairs that will be used as an identical set of options for each ``SELECT`` box, each button sequence, or each radio button sequence.
    Ignored if :py:attr:`options_different` is given a value.
    Each pair consists of ``(short, long)`` where `short` is the token to be used internally (returned as the value submitted by the player when they click on a particular DOM element) and `long` is the object to be displayed to the player: either a string, an image filename, a DOMNode object, or a sequence of arbitrary length containing repeated instances of all three (string, filename, DOMNode, string, filename, ...).
    The allowed types for `long` depend on the type of input DOM element, as follows.

    * InputDOMElementType.SELECTS: `long` is expected to be a text string (or something that can be converted into a str) that is displayed directly as one of the SELECT box items.

    * InputDOMElementType.IMG_SELECTS: `long` is expected to be a filename of a graphic that is displayed directly as one of the SELECT box items.

    * InputDOMElementType.BUTTONS: `long` is expected to be a text string, or something that can be converted into a string (such as an int).

    * InputDOMElementType.IMG_BUTTONS: `long` is expected to be a sequence of arbitrary length containing repeated instances of (text, filename, DOMNode, text, filename, ...). If one is to be skipped, a placeholder of `None` can be used, e.g. ``(None, None, A, None, None, B)`` places DOMNode objects A and B on the button.

    * InputDOMElementType.RADIOBUTTONS: `long` is expected to be either a text string (or something that can be converted into a string) or a DOMNode object (such as a SVG image), but not an image filename encoded as a string. A DOMNode object is the only way to specify an image for a radio button label as the framework does not support image filenames in this role for radio buttons.

    * InputDOMElementType.CHECKBOXES: `long` is expected to be either a text string (or something that can be converted into a string) or a DOMNode object (such as a SVG image), but not an image filename encoded as a string. A DOMNode object is the only way to specify an image for a checkbox label as the framework does not support image filenames in this role for checkboxes.

    * InputDOMElementType.DRAGANDDROP: not relevant; `options` is not expected

    * InputDOMElementType.INT: not relevant; `options` is not expected

    * InputDOMElementType.TEXT: not relevant; `options` is not expected

:py:attr:`options_different`
    seq of iterable of pairs: a
    sequence where each element is as defined in `options` above. Used
    where each ``SELECT`` box, each button sequence, or each radio button
    sequence should have different values. If given a value, :py:attr:`options`
    is ignored.

:py:attr:`cell_border`
    int: the width of the border
    (in px units) around each cell of the table (ignored if evaluates
    to ``False``). Used for radio buttons, for example.

:py:attr:`cell_padding`
    int: the amount of padding
    (in px units) to apply to each cell in a table. Used for radio
    buttons, for example.

:py:attr:`table_style`
    dict: CSS properties to
    modify the style of the table. Used for radio buttons, for
    example.

:py:attr:`cell_style`
    dict: CSS properties to modify
    the style of each cell of the table. Used for radio buttons, for
    example.

:py:attr:`im_height`
    int: height in pixels of any
    images in :py:attr:`options` when used for specific input DOM elements, e.g.
    buttons.


.. _sect_webapp_class_inputdomelementtype:

-----------------------------------------------
The options for :py:class:`InputDOMElementType`
-----------------------------------------------

The class :py:class:`InputDOMElementType` is used as a lightweight enumerate type to specify which DOM elements will solicit the player's answer. It defines the following possibilities. Each game must use exactly one of these options. For example, in the file :file:`justodd/webapp/__init__.py`, in function :py:func:`format_player_answer_area`, it is specified with ``dom_elem_type=InputDOMElementType.CHECKBOXES``.

:py:attr:`INT`
    DOM ``INPUT`` boxes of type 'number'.

:py:attr:`TEXT`
    DOM ``INPUT`` boxes of type 'text'.

:py:attr:`SELECTS`
    Sequence of DOM ``SELECT`` boxes to allow a choice between a set of
    strings.

:py:attr:`IMG_SELECTS`
    Sequence of DOM ``SELECT`` boxes to allow a choice between a set of
    images.

:py:attr:`BUTTONS`
    Sequence of DOM button sequences to allow a choice between a set
    of strings printing values into a text box.

:py:attr:`IMG_BUTTONS`
    Sequence of DOM button sequences to allow a choice between a set
    of string-image pairs printing values into a text box.

:py:attr:`RADIOBUTTONS`
    Sequence of groups of DOM radio buttons to allow a choice between
    a set of DOMNode objects (e.g. SVG objects).

:py:attr:`CHECKBOXES`
    Sequence of of DOM checkboxes to allow a subset of DOMNode
    objects (e.g. SVG objects) to be chosen.

:py:attr:`DRAGANDDROP`
    The problem instance will contain all of the widgets required to
    solicit the player's answer.


.. _sect_webapp_class_inputdomelementoptions:

--------------------------------------------------
The options for :py:class:`InputDOMElementOptions`
--------------------------------------------------

The class :py:class:`InputDOMElementOptions` is used as a lightweight enumerate type to specify options in the list stored in ``InputDOMElements.dom_elem_options``.

For example, in the file :file:`dungeonescape/webapp/__init__.py`, in function :py:func:`format_player_answer_area`, it is specified with ``dom_elem_options=[InputDOMElementOptions.VerticalList]``.

The full list of possibilities is:

:py:attr:`VerticalList`
    Arrange elements vertically.

:py:attr:`HorizontalList`
    Arrange elements horizontally.


--------
Examples
--------

In file :file:`justodd/webapp/__init__.py`, the function :py:func:`format_player_answer_area` returns an object that contains a sequence of options to be displayed as checkboxes.
For checkboxes, the values in :py:attr:`options` must be either text strings or DOMNode objects (e.g. SVG images) and not image filenames.

.. code-block:: python

    input_dom_elements = InputDOMElements(
        dom_elem_type=InputDOMElementType.CHECKBOXES,
        options=options,
        )

In file :file:`dungeonescape/webapp/__init__.py`, the function :py:func:`format_player_answer_area` returns an object that contains a sequence of options to be displayed as a vertical list of radio buttons.
For radio buttons, the values in :py:attr:`options` must be either text strings or DOMNode objects (e.g. SVG images) and not image filenames.

.. code-block:: python

    input_dom_elements = InputDOMElements(
        dom_elem_type=InputDOMElementType.RADIOBUTTONS,
        dom_elem_options=[InputDOMElementOptions.VerticalList],
        options=options,
        )
