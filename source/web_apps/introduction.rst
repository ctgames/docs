.. _chap_web_app_games:

.. centered:: :title:`Chapter: Web app games`

=============================
Introduction to web app games
=============================

The command-line version of a game must be developed before the web app version of the game can be developed. This is covered in the chapters listed in the box below. Following on from those chapters, this chapter provides guidelines for how to turn a CTGames command-line game into a web app, and documents the programming interface for the CTGames framework with respect to web app version of games.

.. seealso::

    Chapter ":ref:`Game development <chap_game_development>`" provides general guidelines for the development of CTGames games.

    Chapter ":ref:`Template game explained <chap_template_game_explained>`" explains how to get started programming a command-line CTGames game.

    Chapter ":ref:`Command-line games <chap_command_line_games>`" documents the programming interface for the CTGames framework with respect to command-line games.


The files that need to be modified to make a web app version of a game (where :file:`x` is the directory name of the game) are

    - :file:`x/webapp/__init__.py` is where the web app front-end of your game is defined, and
    - :file:`x/webapp/names.py` is where the images for the game logo, game icon, and game background are specified, and where the graphical tutorial for your game is defined.
