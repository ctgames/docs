.. _sect_module_svgcommon:

===============================
The module :py:mod:`svgcommmon`
===============================

(defined in :file:`svgcommon.py`)

This section explains the contents of the module :py:mod:`svgcommon`, which is used by web app games to display and control the properties of SVG images, and, indirectly, any other graphical objects (e.g. raster images).

.. note:: If you only require a sequence or regular grid of graphical elements (including images) for your web app, without any overlapping images and without any animation, you do not need any functions from this module can instead just create a :py:class:`TextImageSeq` object as described in section ":ref:`sect_namedtuple_textimageseq`".


.. _sect_module_svgcommon_create_svg_instance:

-------------------------------------------
The function :py:func:`create_svg_instance`
-------------------------------------------

This function returns a new (blank) SVG object into which other SVG objects and images can be positioned, overlaid, and animated.

.. seealso:: Section ":ref:`sect_module_svgcommon_create_image_from_file`" explains how to load an image from a file and insert it into a :py:class:`SVG` DOM object.

To have precise control over the layout of images in a web app game, this function should be called first to create a SVG object. After that, other SVG elements can be added such as images, rectangles, text, and so on.


Parameters of :py:func:`create_svg_instance`
--------------------------------------------

The parameters of :py:func:`create_svg_instance` are as follows.

:py:attr:`id`
    A :py:class:`str`: A :py:class:`str` to be used as the DOM id of this SVG object.

:py:attr:`x`
    An :py:class:`int`, optional, default 0: The x coordinate of the top left corner of this SVG object, relative to its parent SVG (if any).

:py:attr:`y`
    An :py:class:`int`, optional, default 0: The y coordinate of the top left corner of this SVG object, relative to its parent SVG (if any).

:py:attr:`width`
    An :py:class:`int`, optional, default 200: The width of the SVG object.

:py:attr:`height`
    An :py:class:`int`, optional, default 200: The height of the SVG object.


Returned from :py:func:`create_svg_instance`
--------------------------------------------

:py:class:`SVG`
    The function returns an empty :py:class:`SVG` DOMNode object (with ``<svg>`` tag) into which other SVG elements can be added as children.


Examples
--------

In file :file:`justreverse/webapp/__init__.py`, the function :py:func:`format_problem_instance` creates a new blank SVG object that has a height equal to twice the height of :py:data:`IM_BLOCK_SIDE` and width equal to :py:data:`IM_BLOCK_SIDE` times the number of characters in string :py:data:`word`:

.. code-block:: python

    word, animation_type = game_state.custom
    parent_width = IM_BLOCK_SIDE * len(word)

    svg_instance = create_svg_instance(
        id='svg_instance', width=parent_width, height=IM_BLOCK_SIDE * 2
        )


In file :file:`countfromzero/webapp/__init__.py`, the function :py:func:`format_problem_instance` creates a new blank SVG object into which an animated train object will be inserted later. Since the dimensions of the train is not known at this point, only the :py:attr:`id` is specified initially. The :py:attr:`height` and :py:attr:`width` are updated later when the dimensions of the train are known:

.. code-block:: python

    # This parent SVG object will be static in the DOM and its child SVG
    # will be animated.
    svg_instance = create_svg_instance(id='main_svg')

    # Construct the train as a regular SVG and add it to the DOM
    train = _construct_train_svg()
    svg_instance <= train

    # Set the width and height of the parent SVG object to that of the train
    svg_instance.setAttribute('width', train.getAttribute('width'))
    svg_instance.setAttribute('height', train.getAttribute('height'))


.. _sect_module_svgcommon_create_image_from_file:

----------------------------------------------
The function :py:func:`create_image_from_file`
----------------------------------------------

The function :py:func:`create_image_from_file` is used to create a SVG image element using the contents of an external file. This SVG image element is intended to be a child of a SVG instance.

Only JPEG, PNG, and SVG image files are supported.

The image will always preserve its original aspect ratio, and its
dimensions will be determined by whichever of :py:attr:`width` and :py:attr:``height` constricts it the most.

By default (when :py:attr:`inline` is ``False``) when rendered in a browser the SVG image will appear using tag
``<image>`` inside its parent SVG instance (which will have a ``<svg>`` tag). The contents of the image will be rendered in the browser but the programmer does not have access to the image, even if the image is a SVG image.

If :py:attr:`inline` is ``True``, and :py:attr:`href` refers to a SVG image file then the contents of the SVG image file will appear in a ``<svg>`` tag within the parent ``<svg>`` tag. This has the advantage that the programmer has access to the contents of the SVG image and so can change/replace/animate individual parts of the image.

We use the `Brython <https://www.brython.info/>`_ syntax ``<=`` to insert a DOMNode object as a child into an existing DOMNode object. Examples are given below.


Parameters of :py:func:`create_image_from_file`
-----------------------------------------------

The parameters of :py:func:`create_image_from_file` are as follows.

:py:attr:`id`
    A :py:class:`str`: A :py:class:`str` to be used as the DOM id of this SVG image element.

:py:attr:`x`
    An :py:class:`int`: The horizontal position of the image from the origin of the parent SVG.

:py:attr:`y`
    An :py:class:`int`: The vertical position of the image from the origin of the parent SVG.

:py:attr:`width`
    An :py:class:`int`: The maximum width of the image (actual width will depend on the value specified for :py:attr:`height`).

:py:attr:`height`
    An :py:class:`int`: The maximum height of the image (actual height will depend on the value specified for :py:attr:`width`).

:py:attr:`href`
    An :py:class:`str`: The path and filename of the external file.

:py:attr:`angle`
    An :py:class:`int`, optional, default 0: The clockwise angle, in degrees, with which to rotate the SVG image before it is rendered.

:py:attr:`inline`
    A :py:class:`bool`, optional, default ``False``: Whether to insert the image as a link to an external file or as
    an inline SVG document. If ``False``, the DOM tree will consist
    of a href to the graphic. If ``True``, the DOM tree will consist
    of the SVG text itself, which has the advantage of exposing the
    XML tags in the graphic for style changes or animation.

:py:attr:`hidden`
    :py:class:`bool`, optional, default ``False``: Whether to hide the object or not. If ``True`` then the DOM object will not be visible.


These parameters are all stored as attributes of type :py:class:`str` in the SVG image, as is standard for DOMNode objects. If one ever wants to access them later, one can use :py:func:`getAttribute`, for example:

.. code-block:: python

    >>> my_im = create_image_from_file(
            id='cat', x=10, y=20, width=30, height=40, href='my_cat.svg'
            )
    >>> my_im.getAttribute('x')
    10
    >>> my_im.getAttribute('height')
    40
    >>> my_im.getAttribute('href')
    my_cat.svg


Returned from :py:func:`create_image_from_file`
-----------------------------------------------

:py:class:`SVGImageElement`
        The function returns a :py:class:`SVGImageElement` DOMNode object (with ``<image>`` tag). It allows images to be included inside a :py:class:`SVG` DOM object. It can display raster image files (JPEG or PNG) or other SVG files.


Examples
--------

In file :file:`password/webapp/__init__.py`, the function :py:func:`format_problem_instance` creates a :py:class:`SVGImageElement` object using file :file:`game_icon.svg` and adds it to a :py:class:`SVG` object previously created using :py:func:`create_svg_instance`:

.. code-block:: python

    (encoded_password,) = game_state.custom

    svg_instance = create_svg_instance(
        id='svg_instance',
        width=130 * len(encoded_password),
        height=320,
        )

    # Some code is hidden for this example

    box_name = 'game_icon'
    fname = IM_PATH.format(box_name)
    svg_instance <= create_image_from_file(
        id=box_name, x=0, y=0, width=250, height=250, href=fname
        )


In file :file:`justreverse/webapp/__init__.py`, the function :py:func:`format_problem_instance` makes multiple calls to :py:func:`create_image_from_file` to create a list of length `n` of :py:class:`SVGImageElement` objects (with :py:attr:`id` values of "block_0" through "block\_ `n-1`"). Each :py:class:`SVGImageElement` object has the same :py:attr:`width`, :py:attr:`height`, and :py:attr:`y` position, but has a different :py:attr:`x` position that ensures the objects appear side-by-side in the web app:

.. code-block:: python

    word, animation_type = game_state.custom
    parent_width = IM_BLOCK_SIDE * len(word)

    svg_instance = create_svg_instance(
        id='svg_instance', width=parent_width, height=IM_BLOCK_SIDE * 2
        )
    blocks = [
        create_image_from_file(
            id=f'block_{index}',
            x=IM_BLOCK_SIDE * index,
            y=IM_BLOCK_SIDE // 2,
            width=IM_BLOCK_SIDE,
            height=IM_BLOCK_SIDE,
            href=IM_PATH.format(letter.lower()),
            )
        for index, letter in enumerate(word)
        ]
    svg_instance <= blocks

    # Animation code is hidden for this example

    return TextImageSeq(dom_element_seq=[svg_instance])


------------------------------------
The function :py:func:`create_group`
------------------------------------

The function :py:func:`create_group` is used to ... TODO
