.. _chap_student_projects:

.. centered:: Chapter: Student projects

============================
Submitting a student project
============================

CTGames developers are usually students undertaking a final year project at `Maynooth University Department of Computer Science <https://www.cs.nuim.ie/>`_.

The relevant Moodle page has all of the information including the Final Year Project Handbook. This section is intended to supplement that, but if they disagree on any point, Moodle takes precedence. The following information is relevant for students submitting in ``March 2022``:

- Projects should be submitted on Moodle before the end of ``23rd March 2022``

    - Projects consist of a thesis and "Supporting Documentation"

    - For your thesis (sometimes called a project report on Moodle) consider the following:

        - Take a look at the previous years' theses in the Git repository

            - You can adopt the style and layout of the previous theses insofar as they are reasonably compatible with the suggested layout specified in the Final Year Project Handbook, for example, choosing to divide the suggested chapter "The Solution (Design & Implementation)" into two separate chapters "Design" and "Implementation", as several previous students have done, would not be a problem
            - It seems to be a nice touch to include the original versions of your three Bebras tasks in an Appendix of your thesis

        - The thesis page limit given in the Final Year Project Handbook does not include appendices, so you can put as much as you like there
        - Do get someone else to read your report to find all the silly mistakes we all miss in text we write ourselves
        - I can proof-read up to three pages of your report and/or scan through your whole report

            - Email me at least 24 hours before the deadline
            - Tell me the pages you'd like me to read in detail (the abstract and introduction should look quite similar to that in previous year's theses in the Git repo so maybe you'd like me to read sections related to analysis/self-reflection/conclusions/future work)

    - For the "Supporting Documentation" you should include

        - The three directories containing the code for your three games (do not include CTGames framework code or other students' code)
        - Any completed questionnaires from friends and family that you get to evaluate your games  (alternatively, these can go in an appendix  of the thesis if you wish)
        - Record of Meetings Form (alternatively, this can go in an appendix  of the thesis if you wish)

    - For your code, remember these three code-related checklists

        - section ":ref:`sect_copyright_statements`" for details on how to transfer copyright if you want your games to be used by schoolteachers in a classroom setting
        - section ":ref:`sect_checklist_for_game_completion`"
        - section ":ref:`sect_checklist_for_web_app_completion`"

    - For the Record of Meetings Form, if you fill it out using the dates recorded on Teams for our meetings and email it to me, I can give my signature electronically

- Video presentations are due before the end of ``28th March 2022``

    - I recommend you use screen capture software on a computer with a microphone for your video
    - You should record a video that consists of you talking over some presentation slides and switching to demos of your games
    - The presentation slides should touch on the important aspects of your thesis
    - The demos in the video can be brief, for example you might just demo one of the games in detail, and say you can spend more time during the interview

- The project interviews will take place during ``29th March -- 1st April 2022``

    - Your interview will just be an interactive version of the video: you'll present some slides and give live demos of your games
    - You may be asked to show/explain parts of your code
    - Between project submission and the interview you have at least a week to polish your code, consider any issues you didn't address from the three code-related checklists above, and iron out any kinks that might surface during a live demo
