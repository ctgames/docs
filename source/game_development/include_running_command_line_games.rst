Open a terminal in any directory, and enter
::

    conda activate ctgames
    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/bin/
    git pull
    git status

This ensures that your working directory is the CTGames :file:`bin` directory, and that you are in the **ctgames** conda environment. Then, to run any particular game, e.g. Password, enter the directory name of that game
::

    python cli.py password

Over one hundred games are under development using the CTGames framework, some in a more advanced state than others. To list the directory names of all CTGames games under development, enter
::

    python cli.py -l


To get a list of all accepted parameters and options for :file:`cli.py`, enter
::

    python cli.py -h
