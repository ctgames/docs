.. _sect_ctgames_code_style:

==================
CTGames code style
==================

The code style for CTGames is defined in the "blackpp.py" automatic code watcher that is pre-configured in PyCharm in the CTGames VirtualBox appliance.

Briefly, it uses the `black <https://github.com/psf/black>`_ and `isort <https://pycqa.github.io/isort/>`_ code formatters, plus some other minor formatting particular to CTGames.

What this means is that many coding style decisions are taken out of your hands as soon as you hit "Save" in PyCharm.


------------------------------------------------------------------------
Elements of the CTGames style not captured in the automatic code watcher
------------------------------------------------------------------------

#. If there is any doubt about a particular Python style decision, in CTGames development we follow the `PEP 8 <https://www.python.org/dev/peps/pep-0008/>`_ and `PEP 257 <https://www.python.org/dev/peps/pep-0257/>`_ guidelines, such as naming conventions for functions, classes, attributes, filenames, and so on.

#. While lines of code are restricted to lines of width 79 characters, multi-line docstrings and multi-line comments are restricted to line widths of 72 characters.

#. We use single quotes (``'``) almost everywhere in the CTGames codebase. e.g. ``print('Hello!')``. Double quotes are only used

    #. for quotes that appear in the body of a string, e.g. ``print('File "x.txt" not found.')``

    #. for docstrings to document modules, functions, and names, as in:

    .. code-block:: python

        """This module is a test.
        """

        def function():
            """This is a function."""

            CONST: int = 5
            """A very important constant."""
            pass

#. Docstrings that are not used for documentation should use triple single quotes, e.g.:

.. code-block:: python

    err_msg = '''This is a very long error message
        that needs multiple lines, caused by
        function {} in module {}.
        '''.format(
        function_name, module_name
        )

The reason for this is that we want to be able to strip all triple double-quoted docstrings from the code before it is pushed from the web server to the client-side web app (to reduce transmission bandwidth by as much as a third for some modules). Triple single-quoted docstrings will not be stripped.
