.. _sect_creating_a_new_game:

===================
Creating a new game
===================


------------------------------
Create a game using a template
------------------------------

In order to create a new game, first think carefully of a name. It's not easy to change it afterwards. Game names should be in title case (e.g. "Title Case"). The second thing you need to decide is what type of answer do you wish the player to give. This determines which template is chosen for your new game.

There are at least two templates currently available, for cases where we want the player to choose from a list of multiple answers, and for cases where we ask the payer to answer with an integer. To find out the latest selection of templates available in the CTGames framework, open a terminal in any directory and enter::

    conda activate ctgames
    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/bin/
    git pull
    python create_new_game.py --help

As of October 2022, there are two supported templates; if your game will ask multiple-choice questions (MCQs) use `mcq`, otherwise use `other`.

.. tip:: There are only minor differences between the different templates, so you can change your code easily by hand afterwards if you change your mind.

Let's assume the game you wish to create is called "Catch A Ball", and that you want the player to choose from set of predefined possible answers in each round. Then, in the same terminal session that you entered the previous lines, enter::

    python create_new_game.py "Catch A Ball" mcq

Alternatively, if you want the player to enter an integer or a string as their answer for each round, then, in the same terminal session that you entered the previous lines, enter::

    python create_new_game.py "Catch A Ball" other


---------------------------------
The template is a functional game
---------------------------------

The template code for your new game will be created in the games directory on your local computer in a directory called :file:`~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/ctgames/catchaball/`. Although it is not a completed game, it has the functionality of a very simple game, and it runs without errors in the command line and as a web app. So, as you develop, you can be sure any new run-time errors you find are due to your most recent modifications to the code.


------------------------------------------
Pushing your changes to the git repository
------------------------------------------

Make sure to immediately push this newly-created game to the git repository. Then you can easily undo any changes to make. Also, you can clearly see all modifications you make to the template in case you break or delete something. As a reminder, to push the game you just created, in a terminal enter (some lines can be skipped if you've already typed them into this particular terminal session, also type whatever is your own game's directory name in place of :file:`catchaball`)::

    conda activate ctgames
    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/ctgames/
    git pull
    git status
    git add catchaball
    git add ../../../../www/catchaball
    git status
    git commit

.. note:: When you run the :command:`git commit` command, a text editor appears presenting you with a screen of information. Don't delete any existing text in this screen. Under the heading "# Changes to be committed:" this text lists what files you are about to commit. This gives you a valuable opportunity to double-check what you are committing, to ensure that you are not committing any files unrelated to your game, and that you are not committing changes to the CTGames framework unrelated to your own game.

After double checking what files will be committed, type an informative one or two line commit message, press :kbd:`Ctrl-x`, then :kbd:`y` to accept, then press the :kbd:`Enter` key.

Back on the command line, enter::

    git push
    git status

For future commits, you only need to :command:`git add` the files that have changed. The command :command:`git status` will remind you what files you have changed.

..
    **HIDDEN**
    TODO: offer some basic help for the most common errors at the point someone tries to pull or push?


-----------------------------------------------------------
Explaining the code in a game newly-created from a template
-----------------------------------------------------------

The chapter ":ref:`Template game explained <chap_template_game_explained>`" explains the purpose of each part of the code in a game newly-created from a template.
