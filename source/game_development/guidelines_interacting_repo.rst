.. _sect_guidelines_interacting_repo:

==========================================================
Guidelines for interacting with the CTGames git repository
==========================================================

This section is written for git novices that, outside of CTGames development, do not normally use git. For those familiar with git, this section can be summarised by the statement that developers of games in the CTGames framework are encouraged not to push development branches because their code is already separated in the filesystem from other games' (such developers can of course create any local development branches as they see fit, but are requested to merge into the `master` branch before pushing).

.. tip:: One of the most common errors that a git novice receives when they interact with a git repository is "You have divergent branches...".
  This is caused by you trying to modify an out-of-date version of the remote repository that everyone else sees.
  The fix is straightforward: perform a merge.
  However, to avoid this error, each time you want to commit, perform

    - a git pull command, followed by
    - a git commit command, followed by
    - a git push command.

  If you perform these three commands after one another, with as little delay between them as possible, the chances of this error occurring will be minimised.

------------------------------
Restricted set of git commands
------------------------------

You have all been given powerful "Developer" privileges.
You have the power to cause a lot of trouble for your fellow students by changing the git repository contents and/or history.
So, to guard against this, below are the only commands you should use unless you are a git expert.
If you make a mistake using these commands -- no problem, these commands are safe so another CTGames developer will always be able to get you out of trouble.
If you git commands that are not from the subsections below, you will be expected to fix all of your mistakes without me or your fellow students even finding out about it.
Here is a cheat sheet of basic commands and usage from GitLab: https://about.gitlab.com/images/press/git-cheat-sheet.pdf, and this is a good website to learn about these commands: https://git-scm.com/docs (in particular, https://git-scm.com/docs/gittutorial).


Commands
^^^^^^^^

The only commands you actually need are::

    git status
    git pull
    git add my_file.py
    git add my_directory
    git rm
    git mv
    git commit
    git push

.. note:: When you run the :command:`git commit` command, a text editor appears presenting you with a screen of information. Don't delete any existing text in this screen. Under the heading ``# Changes to be committed:`` there is a list of the files you are about to commit. This gives you a valuable opportunity to double-check what you are committing, to ensure that you are not committing any files unrelated to your game, and that you are not committing changes to the CTGames framework unrelated to your own game. After double checking what files will be committed, type an informative one or two line commit message, press :kbd:`Ctrl-x`, then :kbd:`y` to accept, then press the :kbd:`Enter` key.


Additional useful commands
^^^^^^^^^^^^^^^^^^^^^^^^^^

To graphically view the git repo history::

    gitk --all

To add all edits to previously added files in current directory and subdirectories (run :command:`git status` afterwards to see what has been added)::

    git add -u

To add and commit all changes (to both new and modified files) that you have made (run :command:`git status` beforehand to see all files that will be committed)::

    git commit -a

To undo edits you made to a file (important, if the file has been staged/"added" you have to undo that first; also, note, the command  below will delete your edits)::

    git restore my_file.py

To undo an staging/"add" operation::

    git restore --staged my_file.py

To undo the most recent add, but not committed, files/folders (note the full stop/period in this command)::

    git reset .

To undo a commit operation (important, the file should not have been pushed)::

    git reset --soft HEAD^

To undo a push operation::

    There is no satisfactory way to do this with an actively-developed shared repository,
    so please don\'t attempt it.


.. tip:: This can be inferred from above, but it is worth mentioning explicitly. Before running a command such as :command:`git add -u` or :command:`git commit -a` that adds multiple files, run :command:`git status` so that you see the names of the files you are staging for commit -- that way you'll catch if you are about to commit changes to someone else's files. If you have accidentally made a change to someone else's file :file:`my_file.py`, and you haven't yet run :command:`git add`, then running
    ::

        git restore my_file.py

    will delete any changes you accidentally made to that file.


------------------
Guidelines for git
------------------

* Use :command:`git status` before each :command:`git commit` operation to check in advance which files you are about to commit.

* You should :command:`git pull` before commencing your code development each day you plan to work on the project, and in particular immediately before each commit operation. (This will avoid the majority of your potential headaches with git so please make sure to do it.)

* You should :command:`git commit` your changes regularly, and at the very least at the end of each working day.

* You should :command:`git push` immediately after each time you commit. Waiting offers no advantage, but does involve a risk that someone else might modify the same file(s) in the meantime.

* Do not push changes to files in directories outside the directories you create yourself, without discussing with me, as this might affect the running of other games.

* Because, as a game developer, you will restrict your development to your own directories, you can push to `master` (the default branch). By all means create local short-lived (e.g. alive for a day or two at most) branches for experimental development if you wish, however please do not push these branches to the remote repository (more details in section ":ref:`sect_development_branches`").


.. _sect_development_branches:

--------------------
Development branches
--------------------

All CTGames game developers push directly to the "master" branch. This not common among software development projects. The ability to create and push a development branch for one's contributions is one of the most useful features of git, and essential for collaborative development in most projects. Locally, CTGames game developers are encouraged to create development branches if they find them useful, but not to push them to the CTGames remote repository. The overhead is not necessary since each game's code is isolated from other games' code, and the CTGames framework is now quite stable.

Developers of the CTGames framework will push development branches, as their code may affect the running of other developer's games.
