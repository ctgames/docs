.. _chap_game_development:

.. centered:: Chapter: Game development

=======================================
Game development guidelines for CTGames
=======================================

This chapter provides general guidelines for the development of CTGames games. The sections in this chapter are arranged in a suggested reading order.

.. seealso::

    Chapter ":ref:`Template game explained <chap_template_game_explained>`" explains how to get started programming a command-line CTGames game.

    Chapter ":ref:`Command-line games <chap_command_line_games>`" documents the programming interface for the CTGames framework with respect to command-line games.

    Chapter ":ref:`Web app games <chap_web_app_games>`" provides guidelines for how to turn a CTGames command-line game into a web app.


-----------------------
Overview of the chapter
-----------------------


Before starting game development -- one-off tasks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The section ":ref:`sect_before_starting_game_development`" explains tasks that should be performed after a successful installation. It should not be necessary to perform these tasks a second time unless you move your cloned git repository to a different location on your computer.


CTGames code style
^^^^^^^^^^^^^^^^^^

The section ":ref:`sect_ctgames_code_style`" sets out the Python coding style for CTGames developers.


Guidelines for interacting with the CTGames git repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The section ":ref:`sect_guidelines_interacting_repo`" explains how to interact with the CTGames git repository and CTGames framework during game development.


Creating a new game
^^^^^^^^^^^^^^^^^^^

The section ":ref:`sect_creating_a_new_game`"  sets out how to create a new game from a template.


Resources
^^^^^^^^^

The section ":ref:`sect_game_development_resources`" collects resources common to both command-line and web app game development.
